import QtQuick 2.4

Rectangle {
    id: root
    width: 64
    height: 64
    color: "transparent"

    property int rolls: 3


    property alias mouseArea: rootMouseArea

//    function dec(){
//        var value = turnsLefted.text
//        //value.
//    }

    FontLoader { id: fixedFont; source: "qrc:/fonts/Papyrus.ttf" }

    BorderImage {
        id: diceboxImage
        source: "img/dicebox.png"
        width: root.width; height: root.height
        border.left: 1; border.top: 1
        border.right: 1; border.bottom: 1
        Text{   // how many tries left
            id: turnsLefted
            anchors.centerIn: parent
            text: root.rolls
            font.bold: true
            font.pointSize: 20
            font.family: fixedFont.name
        }

        MouseArea{
            id:rootMouseArea
            anchors.fill: parent
//            hoverEnabled: true
//            onEntered: {
//                root.color = "green"
//            }
//            onExited: {
//                root.color = "darkgreen"
//            }
        }
    }




}

