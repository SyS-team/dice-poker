import QtQuick 2.4

DiceBoxForm {
    id: root
    //z: 2

    signal stoppedAnim()
    signal startedAnim()
    //signal startMixingAnimation()

    mouseArea.onReleased: {
        root.scale = 1
        pressedDiceBox()
        //diceBoxParAnim0.running = true

    }
    mouseArea.onPressed: {
        root.scale = 0.95
    }
    onRollsChanged: {
        if(root.rolls===0){
            root.enabled = false
        }
        else if(root.rolls!==0 && root.enabled === false){
            root.enabled = true
        }
    }
    function startMixingAnimation() {
        console.log("startMixingAnimation() RUN")
        //diceBoxParAnim0.running = true
        diceBoxParAnim0.start()
    }

    signal pressedDiceBox()
    signal decRolls()
    signal resetRolls()






    // anim to move dicebox imitating mixing dices(start)
    SequentialAnimation{
        id: diceBoxParAnim0
        running: false
        onStopped: {
            console.log("diceBoxParAnim0.stopped() RUN")
            stoppedAnim()
        }
        onStarted: {
            console.log("diceBoxParAnim0.started() RUN")
            startedAnim()
        }

        SequentialAnimation{
            loops: 3
            ParallelAnimation{
                id:diceBoxParAnim1
                NumberAnimation {
                    target: root
                    property: "x"
                    duration: 75
                    to: root.x +30
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: root
                    property: "y"
                    duration: 75
                    to: root.y -30
                    easing.type: Easing.InOutQuad
                }
            }
            ParallelAnimation{
                id:diceBoxParAnim2
                NumberAnimation {
                    target: root
                    property: "x"
                    duration: 75
                    to: root.x
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: root
                    property: "y"
                    duration: 75
                    to: root.y
                    easing.type: Easing.InOutQuad
                }
            }
            ParallelAnimation{
                id:diceBoxParAnim3
                NumberAnimation {
                    target: root
                    property: "x"
                    duration: 75
                    to: root.x -30
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: root
                    property: "y"
                    duration: 75
                    to: root.y -30
                    easing.type: Easing.InOutQuad
                }
            }
            ParallelAnimation{
                id:diceBoxParAnim4
                NumberAnimation {
                    target: root
                    property: "x"
                    duration: 75
                    to: root.x
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: root
                    property: "y"
                    duration: 75
                    to: root.y
                    easing.type: Easing.InOutQuad
                }
            }
        }

        // two anim to rotate dicebox(end)
        NumberAnimation {
            target: root
            property: "rotation"
            to: 90
            duration: 200
            easing.type: Easing.InOutQuad
        }
        NumberAnimation {
            target: root
            property: "rotation"
            to: 0
            duration: 200
            easing.type: Easing.InOutQuad
        }
    }
}

