import QtQuick 2.4

Rectangle {
    id: root
    width: parent.width //code is working but i don't think it's good idea to use it
    height: parent.height // reciprocally

    opacity: 0
    visible: opacity > 0
    color: "transparent"
    property bool closeable: true // if true close window by clicking on any place

    Rectangle{
        id: background
        anchors.fill: parent
        color: "gray"
        opacity: 0.5
    }

    function show(){
        root.opacity = 1
    }
    function hide(){
        root.opacity = 0
    }
    function nop(){
        // should be another way instead of this s*it
    }

    MouseArea{
        anchors.fill: parent
        onClicked: root.closeable ? hide() : nop()
    }
    Behavior on opacity{
        NumberAnimation {
            duration: 100
            easing.type: Easing.InOutQuad
        }
    }

}

