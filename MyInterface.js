// finds free stack
function findFreeStack (s1,s2,s3,s4,s5){ // OBSOLETE
    if(s1 === false) {
        s1 = true
        return 1
    }
    if(s2 === false) {
        s2 = true
        return 2
    }
    if(s3 === false) {
        s3 = true
        return 3
    }
    if(s4 === false) {
        s4 = true
        return 4
    }
    if(s5 === false) {
        s5 = true
        return 5
    }
}
function findFreeStack2 (ref){
    if(ref.firstStack === false) {
        ref.firstStack = true// подразумевается что эта функция использ только для последующей отправки dice'а в stack
        return 1
    }
    if(ref.secondStack === false) {
        ref.secondStack = true
        return 2
    }
    if(ref.thirdStack === false) {
        ref.thirdStack = true
        return 3
    }
    if(ref.fourthStack === false) {
        ref.fourthStack = true
        return 4
    }
    if(ref.fivesStack === false) {
        ref.fivesStack = true
        return 5
    }
    return 4
}

//checks dice: is it in stack or not.
function checkDice(isInStack){
    if(isInStack === true) return 1
    else return 0
}

// move to stack if dice is NOT in stack
// P.S. only values, not animation
function moveToStack(diceRef, findedStack, stackPanelRef){
    //diceRef.isInStack = true
    diceRef.stackNumber = findedStack
    if(findedStack === 1){
        diceRef.freeStackRef = stackPanelRef//.stack1
        diceRef.x = stackPanelRef.stack1.realX
        diceRef.y = stackPanelRef.stack1.realY
    }
    else if(findedStack === 2){
        diceRef.freeStackRef = stackPanelRef//.stack2
        diceRef.x = stackPanelRef.stack2.realX
        diceRef.y = stackPanelRef.stack2.realY
    }
    else if(findedStack === 3){
        diceRef.freeStackRef = stackPanelRef//.stack3
        diceRef.x = stackPanelRef.stack3.realX
        diceRef.y = stackPanelRef.stack3.realY
    }
    else if(findedStack === 4){
        diceRef.freeStackRef = stackPanelRef//.stack4
        diceRef.x = stackPanelRef.stack4.realX
        diceRef.y = stackPanelRef.stack4.realY
    }
    else if(findedStack === 5){
        diceRef.freeStackRef = stackPanelRef//.stack5
        diceRef.x = stackPanelRef.stack5.realX
        diceRef.y = stackPanelRef.stack5.realY
    }
}

// moves from stack if dice IS in stack
function moveFromStack(diceRef, stackPanelRef){
    var sN = diceRef.stackNumber
    diceRef.stackNumber = 0
    //diceRef.isInStack = false
    switch(sN){
    case 1: stackPanelRef.firstStack = false; break
    case 2: stackPanelRef.secondStack = false; break
    case 3: stackPanelRef.thirdStack = false; break
    case 4: stackPanelRef.fourthStack = false; break
    case 5: stackPanelRef.fivesStack = false; break
    }

}

// generate random coords on playField for dice
function getRandCoords(playFieldX, playFieldY, playFieldRef, diceRef){
    var coord = {x: 0, y:0}
    var minX = playFieldX//+diceRef.width/2
    var maxX = playFieldX+playFieldRef.width-diceRef.width
    var minY = playFieldY
    var maxY = playFieldY+playFieldRef.height-diceRef.height
    coord.x = Math.floor(Math.random() * (maxX - minX + 1) + minX)
    coord.y = Math.floor(Math.random() * (maxY - minY + 1) + minY)

    // set coords immediatly. <?>rename function</?>
    diceRef.x = coord.x
    diceRef.y = coord.y

    return coord
}
// another way to generate rand coord without setting it
function getRandCoords2(playFieldRef,diceRef){
    var coord = {x: 0, y:0}
    var minX = 0
    var minY = 0
    var maxX = playFieldRef.width-diceRef.width
    var maxY = playFieldRef.height - diceRef.height
    coord.x = Math.floor(Math.random() * (maxX - minX + 1)) + minX
    coord.y = Math.floor(Math.random() * (maxY - minY + 1)) + minY
    return coord
}
function getRandCells3(playFieldRef,diceRef){
    //var coord = []
    //coord[0] = {x: 0, y:0};coord[1] = {x: 0, y:0};coord[2] = {x: 0, y:0};
    //coord[3] = {x: 0, y:0};coord[4] = {x: 0, y:0};
    //var minX = 0
    //var minY = 0
    //var maxX = playFieldRef.width-diceRef.width
    //var maxY = playFieldRef.height - diceRef.height
    //coord.x = Math.floor(Math.random() * (maxX - minX + 1)) + minX
    //coord.y = Math.floor(Math.random() * (maxY - minY + 1)) + minY

    //var dv = diceRef.width
    var cellsMatrix = []        // matrix of cell's numbers

    for(var i=0;i<5;++i){       // random filling of matr by 5 random different values
        cellsMatrix[i] = Math.floor( Math.random() * 25 )     // generate random [1;25]
        if(i===0)continue                       // first elem of matr already is unic
        else{                                   // others needs to check
            if(cellsMatrix.indexOf(cellsMatrix[i])===i)continue     // if first founded is generated on this round OK
            else --i                                                // then regenerate number in same array cell
        }
    }
    //var indX, indY, cellMinX, cellMinY, cellMaxX, cellMaxY
    //var koefX = Math.floor(playFieldRef.width/5)
    //var koefY = Math.floor(playFieldRef.height/5)
    return cellsMatrix
}
function getRandCoordInCell(cell,playFieldRef,diceRef){
    var coord = {x:0,y:0}
    var indX, indY, cellMinX, cellMinY, cellMaxX, cellMaxY
    var koefX = Math.floor(playFieldRef.width/5)
    var koefY = Math.floor(playFieldRef.height/5)
    indY = Math.floor(cell / 5)
    indX = (cell%5)
    console.log("cell:"+cell+"; indY:"+indY+" ;indX:"+indX)
    cellMinX = koefX * indX// +2 // +2 and -2 pixel to avoid collision
    cellMaxX = cellMinX+koefX -diceRef.width// -4 // -4 because we add 2 in cellMin
    cellMinY = koefY * indY// +2
    cellMaxY = cellMinY+koefY -diceRef.height// -4
    console.log(cellMinX+","+cellMaxX+","+cellMinY+","+cellMaxY)
    coord.x = Math.floor(Math.random() * (cellMaxX - cellMinX + 1) + cellMinX )
    coord.y = Math.floor(Math.random() * (cellMaxY - cellMinY + 1) + cellMinY )
    console.log(coord.x+","+coord.y)
    return coord
}

function findFreeCellOnField(dice1,dice2,dice3,dice4,dice5){
    var cells = []
    cells[0] = dice1.playFieldCell
    cells[1] = dice2.playFieldCell
    cells[2] = dice3.playFieldCell
    cells[3] = dice4.playFieldCell
    cells[4] = dice5.playFieldCell
    while(true){
        cells[5] = Math.floor( Math.random() * 25 )     // generate random [1;25]
        if(cells.indexOf(cells[5])===5)break     // if first founded is generated on this round OK
    }
    return cells[5]
}


// generate rand coords in playfield
//function getRandCoords3(playFieldRef,diceRef){
//    var coord = {x: 0, y:0}
//    var minX = 0
//    var minY = 0
//    var maxX = playFieldRef.width-diceRef.width
//    var maxY = playFieldRef.height - diceRef.height
//}



