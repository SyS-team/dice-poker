#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <ctime>
#include <QTranslator>
#include "dicesdata.h"
#include "dice.h"
#include "combination.h"


int main(int argc, char *argv[])
{
    qsrand(time(0));
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    QTranslator translator;
    QString locale = QLocale::system().name();
    translator.load("translation_"+locale,app.applicationDirPath()+"/localisation");
    app.installTranslator(&translator);


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    engine.setProperty("width",200);

    DicesData dicesData(engine.rootObjects().first());
    Combination comb;
    //Dice dice;
    QQmlContext * qqc = engine.rootContext();
    qqc->setContextProperty("DicesData", &dicesData);
    qqc->setContextProperty("CombClass",&comb);
    //qqc->setContextProperty("DiceCpp",&dice);
    //qmlRegisterType<DicesData>("com.ss.dice",1,0,"DicesData");
    //qmlRegisterType<Dice>("com.ss.dice",1,0,"DiceCpp");
    //qmlRegisterType("qrc:/qml/dice/Dice.qml",1,0,"Dice.qml");
//
    //
    //
    //
    //
    //
    //
    //
    //
//
    //
    //
    return app.exec();
}
