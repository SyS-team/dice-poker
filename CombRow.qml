import QtQuick 2.4

CombRowForm {
    id: root
    property string text: "Text"
    //property string points1: "0"
    //property string points2: "0"


    signal setPoints1()
    signal setPoints2()

    function resetPoints(){
        root.points1 = 0
        root.points2 = 0
        root.state = "00"
    }

    states: [
        State{
            name: "10"
            PropertyChanges {
                target: textPoints1
                font.bold: true
                color: "red"
            }
            PropertyChanges {
                target: mouseAreaPoints1
                enabled: false
                hoverEnabled: false

            }
            PropertyChanges {
                target: root
                points1Entered: true
            }
        },
        State{
            name: "01"
            PropertyChanges {
                target: textPoints2
                font.bold: true
                color: "red"
            }
            PropertyChanges {
                target: mouseAreaPoints2
                enabled: false
                hoverEnabled: false

            }
            PropertyChanges {
                target: root
                points2Entered: true
            }
        },
        State{
            name: "11"
            PropertyChanges {
                target: textPoints1
                font.bold: true
                color: "red"
            }
            PropertyChanges {
                target: mouseAreaPoints1
                enabled: false
                hoverEnabled: false

            }
            PropertyChanges {
                target: root
                points1Entered: true
            }
            PropertyChanges {
                target: textPoints2
                font.bold: true
                color: "red"
            }
            PropertyChanges {
                target: mouseAreaPoints2
                enabled: false
                hoverEnabled: false

            }
            PropertyChanges {
                target: root
                points2Entered: true
            }
        },
        State{
            name: "00"
            PropertyChanges {
                target: textPoints1
                font.bold: false
                color: "black"
            }
            PropertyChanges {
                target: mouseAreaPoints1
                enabled: true
                hoverEnabled: true

            }
            PropertyChanges {
                target: root
                points1Entered: false
            }
            PropertyChanges {
                target: textPoints2
                font.bold: false
                color: "black"
            }
            PropertyChanges {
                target: mouseAreaPoints2
                enabled: true
                hoverEnabled: true

            }
            PropertyChanges {
                target: root
                points2Entered: false
            }
        }

    ]


    //points1
    mouseAreaPoints1.onEntered: {
        textPoints1.color = "green"
        textPoints1.font.bold = true
    }
    mouseAreaPoints1.onExited: {
        textPoints1.color = "black"
        textPoints1.font.bold = false
        if(root.points1Entered){// need for true %color_name% color when combin was clicked
            textPoints1.font.bold = true
            textPoints1.color = "red"
        }
    }

    mouseAreaPoints1.onClicked:{
        setPoints1()
        if(root.points2Entered)root.state = "11"
        else root.state = "10"
        //root.points1Entered = true
    }
    // points2
    mouseAreaPoints2.onEntered: {
        textPoints2.color = "green"
        textPoints2.font.bold = true
    }
    mouseAreaPoints2.onExited: {
        textPoints2.color = "black"
        textPoints2.font.bold = false
        if(root.points2Entered){// need for true %color_name% color when combin was clicked
            textPoints2.font.bold = true
            textPoints2.color = "red"
        }
    }

    mouseAreaPoints2.onClicked:{
        setPoints2()
        if(root.points1Entered)root.state = "11"
        else root.state = "01"
//        textPoints2.font.bold = true
//        textPoints2.color = "red"
//        mouseAreaPoints2.enabled = false
//        mouseAreaPoints2.hoverEnabled = false
//        root.points2Entered = true
    }



}

