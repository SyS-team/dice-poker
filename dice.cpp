///////////////////////////////////////////////////////////
//  Dice.cpp
//  Implementation of the Class Dice
//  Created on:      16-май-2015 18:57:57
//  Original author: z510
///////////////////////////////////////////////////////////

#include "dice.h"
#include <cmath>
#include <cstdlib>
#include <ctime>

Dice::Dice()
{

}
Dice::Dice(int coordX, int coordY){
    this->m_coordX = coordX;
    this->m_coordY = coordY;
}

Dice::~Dice()
{

}

void Dice::setCoordX(int x){
    m_coordX = x;
    // сейчас будет жуткий быдлокод, но на стадии обучения пока только так

}


void Dice::setCoordY(int y){
    m_coordY = y;
}


void Dice::setValue(int value){
    this->m_value = value;
}


void Dice::setUsed(bool used){
    this->used = used;
}


int Dice::getCoordX(){

    return this->m_coordX;
}


int Dice::getValue(){

    return this->m_value;
}


int Dice::getCoordY(){

    return this->m_coordY;
}


bool Dice::getUsed(){

    return this->used;
}

// generate random coordinate X
int Dice::randomCoordX(){
    int resultX;
    int minX = PLAY_FIELD_X;
    int maxX = PLAY_FIELD_X+PLAY_FIELD_W-DICE_WIDTH;

    srand(time(NULL));
    resultX = rand()%(maxX-minX)+minX;
    return resultX;
}
// generate random coordinate Y
int Dice::randomCoordY(){
    int resultY;
    int minY = PLAY_FIELD_Y;
    int maxY = PLAY_FIELD_Y+PLAY_FIELD_H-DICE_HEIGHT;
    //qsrand(time(0));
    resultY = qrand()%(maxY-minY)+minY;
    return resultY;
}
// generate random value
int Dice::randomValue(){
    int res;
    //qsrand(time(0));
    res = qrand()%6+1;
    return res;
}


