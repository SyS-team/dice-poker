import QtQuick 2.4

Rectangle {
    id: root
    width: 304
    height: 24
    color: "transparent"
    border.width: 1
    radius: 10
    border.color: "#00000000"

    property int points1: 0
    property int points2: 0
    property int prePoints1: 0
    property int prePoints2: 0
    property bool points1Entered: false
    property bool points2Entered: false
    property string combName: "Combination" //: textCombName.text

    property alias textPoints1: textPoints1
    property alias textPoints2: textPoints2
    property alias mouseAreaPoints1: mouseAreaPoints1
    property alias mouseAreaPoints2: mouseAreaPoints2

    FontLoader { id: fixedFont; source: "qrc:/fonts/Papyrus.ttf" }


    signal clickedPoints()
//    clickedPoints:

    Rectangle {
        id: rectangle1
        color: "#00000000"
        radius: 12
        anchors.rightMargin: 10
        anchors.leftMargin: 13
        border.color: "#a52a2a"
        anchors.fill: parent
    }

    //FontLoader{id:fixedFont; name: "Papyrus"}

    Text{
        id: textCombName
        width: 176-18-64
        //root.width*2/3
        text: root.combName
        font.bold: true
        font.pointSize: 12
        font.family: fixedFont.name
        anchors.left: parent.left
        anchors.leftMargin: 18
        anchors.verticalCenter: parent.verticalCenter //"combin"
        // TODO styles
    }
    Text{
        id: textPoints1
        width: 96 //root.width/3
        anchors.left: textCombName.right
        text: (root.points1Entered===true) ? root.points1 : root.points1+root.prePoints1
        font.pointSize: 12
        font.family: fixedFont.name
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        // TODO styles

        MouseArea{
            id: mouseAreaPoints1
            anchors.fill: parent
            hoverEnabled: true
        }
    }

    Text{
        id: textPoints2
        width: 96 //root.width/3
        anchors.left: textPoints1.right
        text: (root.points2Entered===true) ? root.points2 : root.points2+root.prePoints2
        font.pointSize: 12
        font.family: fixedFont.name
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        // TODO styles

        MouseArea{
            id: mouseAreaPoints2
            anchors.fill: parent
            hoverEnabled: true
        }
    }

}

