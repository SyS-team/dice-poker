import QtQuick 2.4

Rectangle {
    id: root
    width: 304
    height: 400
    //color: "#fff59d"
    color: "transparent"

    property alias nameRow: nameRow1
    property alias combRow1: combRow1
    property alias combRow2: combRow2
    property alias combRow3: combRow3
    property alias combRow4: combRow4
    property alias combRow5: combRow5
    property alias combRow6: combRow6
    property alias combRow7: combRow7
    property alias combRow8: combRow8
    property alias combRow9: combRow9
    property alias combRow10: combRow10
    property alias combRow11: combRow11
    property alias combRow12: combRow12
    property alias totalRow: totalRow1

    Image{
        id: image1
        anchors.fill: parent
        source: "qrc:/img/comb_panel.png"
    }

    Column {
        id: column1
        anchors.fill: parent


        NameRow {
            id: nameRow1
        }

        CombRow {
            id: combRow1
            combName: qsTr("Ones")
        }

        CombRow {
            id: combRow2
            combName: qsTr("Twos")
        }

        CombRow {
            id: combRow3
            combName: qsTr("Threes")
        }

        CombRow {
            id: combRow4
            combName: qsTr("Fours")
        }

        CombRow {
            id: combRow5
            combName: qsTr("Fives")
        }

        CombRow {
            id: combRow6
            combName: qsTr("Sixes")
        }

        CombRow {
            id: combRow7
            combName: qsTr("Four of a kind")
        }

        CombRow {
            id: combRow8
            combName: qsTr("Full house")
        }

        CombRow {
            id: combRow9
            combName: qsTr("Small Straight")
        }

        CombRow {
            id: combRow10
            combName: qsTr("Large Straight")
        }

        CombRow {
            id: combRow11
            combName: qsTr("Yahtzee")
        }

        CombRow {
            id: combRow12
            combName: qsTr("Chance")
        }

        TotalRow {
            id: totalRow1
        }

    }
}

