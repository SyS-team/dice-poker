import QtQuick 2.4

Rectangle {
    id: root
    width: 464
    height: 96
    color: "transparent"


    property alias stack1: stack1
    property alias stack2: stack2
    property alias stack3: stack3
    property alias stack4: stack4
    property alias stack5: stack5


    Rectangle{
        id: rectWithMargins
        width: 464-4 // 4dp to margins
        height: 96-4
        anchors.centerIn: parent
        color: "transparent"
        //color: "#FF5252"
        Item { // IS NOT STACK. ONLY PART OF STACKPANEL
            id: part1
            width: 92
            height: 92
            Image {
                id: image1
                anchors.fill: parent
                source: "qrc:/img/img/stack.png"
            }
            Rectangle{
                id: stack1
                width: 72
                height: 72
                visible: false
                property int realX: stack1.x+2
                property int realY: stack1.y+2
                anchors.centerIn: parent
                color: "#D50000"
            }
        }

        Item {
            id: part2
            width: 92
            height: 92
            anchors.left: part1.right
            Image {
                id: image2
                anchors.fill: parent
                source: "qrc:/img/img/stack.png"
            }
            Rectangle{
                id: stack2
                width: 72
                height: 72
                visible: false
                property int realX: stack2.x+2+part2.x
                property int realY: stack2.y+2
                anchors.centerIn: parent
                color: "#D50000"
            }
        }

        Item {
            id: part3
            width: 92
            height: 92
            anchors.left: part2.right
            Image {
                id: image3
                anchors.fill: parent
                source: "qrc:/img/img/stack.png"
            }
            Rectangle{
                id: stack3
                width: 72
                height: 72
                visible: false
                property int realX: stack3.x+2+part3.x
                property int realY: stack3.y+2
                anchors.centerIn: parent
                color: "#D50000"
            }
        }

        Item {
            id: part4
            width: 92
            height: 92
            anchors.left: part3.right
            Image {
                id: image4
                anchors.fill: parent
                source: "qrc:/img/img/stack.png"
            }
            Rectangle{
                id: stack4
                width: 72
                height: 72
                visible: false
                property int realX: stack4.x+2+part4.x
                property int realY: stack4.y+2
                anchors.centerIn: parent
                color: "#D50000"
            }
        }

        Item {
            id: part5
            width: 92
            height: 92
            anchors.left: part4.right
            Image {
                id: image5
                anchors.fill: parent
                source: "qrc:/img/img/stack.png"
            }
            Rectangle{
                id: stack5
                width: 72
                height: 72
                visible: false
                property int realX: stack5.x+2+part5.x
                property int realY: stack5.y+2
                anchors.centerIn: parent
                color: "#D50000"
            }
        }
    }


}

