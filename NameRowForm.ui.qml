import QtQuick 2.4

Item {
    id: item1
    width: 304
    height: 24
    property alias name1: textName1.text
    property alias name2: textName2.text

    FontLoader { id: fixedFont; source: "qrc:/fonts/Papyrus.ttf" }

    Text{ // OBSOLETE
        id: textCombName
        width: 304-96-96
        //root.width*2/3
        text: ""
        anchors.verticalCenter: parent.verticalCenter //"combin"
        // TODO styles
    }
    Text{
        id: textName1
        width: 96
        height: 16 //root.width/3
        anchors.left: textCombName.right
        text: qsTr("Player 1")
        anchors.verticalCenterOffset: 0
        anchors.leftMargin: 0
        style: Text.Outline
        styleColor: "#068709"
        font.family: fixedFont.name
        font.bold: true
        font.pointSize: 11
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        // TODO styles

    }
    Text{
        id: textName2
        width: 96
        height: 16
        //root.width/3
        anchors.left: textName1.right
        text: qsTr("Player 2")
        font.pointSize: 11
        style: Text.Outline
        styleColor: "#068709"
        font.family: fixedFont.name
        font.bold: true
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        // TODO styles
    }
}

