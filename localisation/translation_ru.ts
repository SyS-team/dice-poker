<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CombinationPanelForm.ui</name>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="42"/>
        <source>Ones</source>
        <translation>Единицы</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="47"/>
        <source>Twos</source>
        <translation>Двойки</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="52"/>
        <source>Threes</source>
        <translation>Тройки</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="57"/>
        <source>Fours</source>
        <translation>Четверки</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="62"/>
        <source>Fives</source>
        <translation>Пятерки</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="67"/>
        <source>Sixes</source>
        <translation>Шестерки</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="72"/>
        <source>Four of a kind</source>
        <translation>Каре</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="77"/>
        <source>Full house</source>
        <translation>Фулл хаус</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="82"/>
        <source>Small Straight</source>
        <translation>Короткий Стрит</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="87"/>
        <source>Large Straight</source>
        <translation>Длинный Стрит</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="92"/>
        <source>Yahtzee</source>
        <translation>Покер</translation>
    </message>
    <message>
        <location filename="../CombinationPanelForm.ui.qml" line="97"/>
        <source>Chance</source>
        <translation>Шанс</translation>
    </message>
</context>
<context>
    <name>MenuPanelForm.ui</name>
    <message>
        <location filename="../MenuPanelForm.ui.qml" line="47"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="../MenuPanelForm.ui.qml" line="56"/>
        <source>Give Up</source>
        <translation>Сдаться</translation>
    </message>
    <message>
        <location filename="../MenuPanelForm.ui.qml" line="65"/>
        <source>Rules</source>
        <translation>Правила</translation>
    </message>
    <message>
        <location filename="../MenuPanelForm.ui.qml" line="74"/>
        <source>About</source>
        <translation>Об игре</translation>
    </message>
    <message>
        <location filename="../MenuPanelForm.ui.qml" line="83"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>NameRowForm.ui</name>
    <message>
        <location filename="../NameRowForm.ui.qml" line="25"/>
        <source>Player 1</source>
        <translation>Игрок 1</translation>
    </message>
    <message>
        <location filename="../NameRowForm.ui.qml" line="44"/>
        <source>Player 2</source>
        <translation>Игрок 2</translation>
    </message>
</context>
<context>
    <name>StartPanel</name>
    <message>
        <location filename="../StartPanel.qml" line="8"/>
        <source>Player 1</source>
        <translation>Игрок 1</translation>
    </message>
    <message>
        <location filename="../StartPanel.qml" line="9"/>
        <source>Player 2</source>
        <translation>Игрок 2</translation>
    </message>
</context>
<context>
    <name>StartPanelForm.ui</name>
    <message>
        <location filename="../StartPanelForm.ui.qml" line="44"/>
        <source>Enter Player 1</source>
        <translation>Игрок 1</translation>
    </message>
    <message>
        <location filename="../StartPanelForm.ui.qml" line="60"/>
        <source>Enter Name 1</source>
        <translation>Введите имя 1</translation>
    </message>
    <message>
        <location filename="../StartPanelForm.ui.qml" line="79"/>
        <source>Enter Player 2</source>
        <translation>Игрок 2</translation>
    </message>
    <message>
        <location filename="../StartPanelForm.ui.qml" line="95"/>
        <source>Enter Name 2</source>
        <translation>Введите имя 2</translation>
    </message>
    <message>
        <location filename="../StartPanelForm.ui.qml" line="112"/>
        <source>Apply</source>
        <translation>Подтвердить</translation>
    </message>
</context>
<context>
    <name>TotalRowForm.ui</name>
    <message>
        <location filename="../TotalRowForm.ui.qml" line="10"/>
        <source>Total score</source>
        <translation>Всего</translation>
    </message>
</context>
<context>
    <name>WinnerModalForm.ui</name>
    <message>
        <location filename="../WinnerModalForm.ui.qml" line="23"/>
        <source> win!</source>
        <comment>winner message</comment>
        <translation> победил!</translation>
        <extra-Context>Full string is &quot;@playername win!&quot; Ex: &quot;Player win!&quot;</extra-Context>
    </message>
    <message>
        <location filename="../WinnerModalForm.ui.qml" line="36"/>
        <source>Round draw!</source>
        <translation>Ничья!</translation>
    </message>
</context>
</TS>
