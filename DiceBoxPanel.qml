import QtQuick 2.4


DiceBoxPanelForm {
    id: root
    //diceBoxState: "First"
    color: "transparent"

    signal stoppedAnim()
    signal startedAnim()
    //signal startMixingAnimation()
    signal pressedDiceBox()
    signal decRolls()
    signal resetRolls()
    signal rollsChanged()

    diceBox.onStoppedAnim: stoppedAnim()
    diceBox.onStartedAnim: startedAnim()

    diceBox.onPressedDiceBox: pressedDiceBox()
    diceBox.onDecRolls: decRolls()
    diceBox.onResetRolls: resetRolls()
    diceBox.onRollsChanged: rollsChanged()
    //diceBox.onStartMixingAnimation: startMixingAnimation()





    function changePlayer(player){
        switch(player){
        case 1: root.state = "First"; break
        case 2: root.state = "Second"; break
        }
    }

    Behavior on diceBox.x {

        NumberAnimation {
            duration: 200
            easing.type: Easing.InOutQuad
            //onStopped: root.diceBoxState === "First" ? root.diceBoxState = "Second" : root.diceBoxState = "First"
        }
    }
}

