# README #


### What is this repository for? ###

* This is a game like Yahtzee known in russian as "Dice Poker" or "Dices"
* v0.92-beta

### Game rules ###

The game consists of a number of rounds. In each round, a player gets three rolls of the dice, although they can choose to end their turn after one or two rolls. After the first roll the player can save any dice they want and re-roll the other dice. This procedure is repeated after the second roll. The player has complete choice as to which dice to roll. They can re-roll a dice for the third roll that was not rolled on the second roll.

The Yahtzee scorecard contains 13 different category boxes and in each round, after the third roll, the player must choose one of these categories. The score entered in the box depends on how well the five dice match the scoring rule for the category. Details of the scoring rules for each category are given below. As an example, one of the categories is called Three-of-a-Kind. The scoring rule for this category means that a player only scores if at least three of the five dice are the same value. The game is completed after 12 rounds by each player, with each of the 12 boxes filled. The total score is calculated by summing all twelve boxes, together.

The Yahtzee scorecard contains 13 scoring boxes divided between two sections: the **upper section** and the **lower section**.

Source: [Read more in  Wikipedia](http://en.wikipedia.org/wiki/Yahtzee)

### Download ###
[https://bitbucket.org/SyS-team/dice-poker/downloads](https://bitbucket.org/SyS-team/dice-poker/downloads)


### Who do I talk to? ###

* https://bitbucket.org/JuliCra