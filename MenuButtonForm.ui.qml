import QtQuick 2.4

Rectangle {
    id: root
    width: 100
    height: 30
    color: "darkgreen"
    border.color: "brown"
    border.width: 3
    radius: 10

    property alias mouseAreaButton: mouseAreaButton
    property alias text: textButton.text

    //signal pressedUser()
    //onPressedUser: ;

    FontLoader { id: fixedFont; source: "qrc:/fonts/Papyrus.ttf" }

    MouseArea{
        id:mouseAreaButton
        anchors.fill: parent
        hoverEnabled: true
    }
    Text{
        id:textButton
        anchors.centerIn: parent
        color: "white"
        font.bold: true
        font.pointSize: 14//(root.width/textButton.text.length)*0.9
        font.family: fixedFont.name
        text: "Button"
        // TODO correct styles
    }
}
