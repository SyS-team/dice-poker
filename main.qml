import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
//import "qrc:/qml/dice/*" as DiceDir
//import com.ss.dice 1.0
import "MyInterface.js" as MyScript
//import "qrc:/dice/Dice.qml" as Dice
//import "qrc:/modal/StartGame.qml" as StartGame
////
Window {
    id: root
    visible: true
    width: 768
    height: 576

    maximumWidth: 768
    maximumHeight: 576
    minimumWidth: 768
    minimumHeight: 576



    MainForm{
        id: mainFormUnit
        width: 768
        height: 576
        z: 1
        Component.onCompleted: {
            combinationPanel.blockAllPoints()
            diceBoxPanel.diceBox.enabled = false
            blockDices(true)
            mainFormUnit.menuPanel.buttonGiveup.disabled = true
            menuPanel.z = 2
            combinationPanel.z = 2
            stackPanel.z = 2
            // z = 3 reserved for dice in dicebox // did not work???
            diceBoxPanel.z = 6

            var pfX = mainFormUnit.playField.x
            var pfY = mainFormUnit.playField.y
            //MyScript.moveFromStack(dice1,mainFormUnit.stackPanel)

            dice1.visible = false; dice2.visible = false;
            dice3.visible = false; dice4.visible = false; dice5.visible = false;
            var initCells = []
            initCells = MyScript.getRandCells3(mainFormUnit.playField,dice1)
            dice1.playFieldCell = initCells[0]
            dice2.playFieldCell = initCells[1]
            dice3.playFieldCell = initCells[2]
            dice4.playFieldCell = initCells[3]
            dice5.playFieldCell = initCells[4]
            dice1.visible = true;dice2.visible = true;
            dice3.visible = true;dice4.visible = true;dice5.visible = true;

            //var coord = MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice1)
            //MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice2)
            //MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice3)
            //MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice4)
            //MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice5)
        }

        property int closedCombs: 0
        property int currentPlayer: 1
        property int exDicesInDicebox: 00000
        property int realDicesInDicebox: 00000

        onRealDicesInDiceboxChanged: {
            console.log("realDicesInDiceboxChaged() RUN")
            if(realDicesInDicebox === exDicesInDicebox && exDicesInDicebox !== 0){
                console.log("rDIB === eDIB && eDIB!== 0")
                diceBoxPanel.diceBox.startMixingAnimation()
            }
            console.log("realDicesInDicebox changes to: "+realDicesInDicebox)
        }

        onCurrentPlayerChanged: {
            if(diceBoxPanel.diceBox.rolls!==3)combinationPanel.blockPoints(currentPlayer)
            else combinationPanel.blockAllPoints()
        }

        function startGame(){
            closedCombs = 0
            currentPlayer = 1
            diceBoxPanel.resetRolls()
            diceBoxPanel.state = "First"
            combinationPanel.resetData()
            mainFormUnit.menuPanel.buttonStart.disabled = true
            mainFormUnit.menuPanel.buttonGiveup.disabled = false
            combinationPanel.blockPoints(currentPlayer)
            diceBoxPanel.diceBox.enabled = true
            //blockDices(true)

        }
        function blockDices(bool){
            dice1.enabled = !bool
            dice2.enabled = !bool
            dice3.enabled = !bool
            dice4.enabled = !bool
            dice5.enabled = !bool
        }

        function clearData(){
            combinationPanel.resetData()
        }

        function endGame(){
            var score1 = combinationPanel.totalRow.points1
            var score2 = combinationPanel.totalRow.points2
            winnerPanel.setScores(score1, score2)
            if(score1>score2){
                winnerPanel.setWinnerName(combinationPanel.nameRow.name1)
            }
            else if(score1<score2){
                winnerPanel.setWinnerName(combinationPanel.nameRow.name2)
            }
            else{
                winnerPanel.gameDraw()
            }

            winnerPanel.show()
            mainFormUnit.menuPanel.buttonStart.disabled = false
            clearData()

        }
        signal diceInDicebox();
        signal diceOutDicebox();
        onDiceInDicebox:{
            //scaling dices to covering them by dicebox
            dice1.scale = 0.2
            dice2.scale = 0.2
            dice3.scale = 0.2
            dice4.scale = 0.2
            dice5.scale = 0.2
            // parent item  --- dicebox
            dice1.parent = mainFormUnit.diceBoxPanel.diceBox
            dice2.parent = mainFormUnit.diceBoxPanel.diceBox
            dice3.parent = mainFormUnit.diceBoxPanel.diceBox
            dice4.parent = mainFormUnit.diceBoxPanel.diceBox
            dice5.parent = mainFormUnit.diceBoxPanel.diceBox
            // coords of dice 'under' dicebox
            dice1.x = 10;dice2.x = 10;dice3.x = 10;dice4.x = 10;dice5.x = 10;
            dice1.y = 10;dice2.y = 10;dice3.y = 10;dice4.y = 10;dice5.y = 10;
        }
        onDiceOutDicebox:{
            // return normal size of dices
            dice1.scale = 1
            dice2.scale = 1
            dice3.scale = 1
            dice4.scale = 1
            dice5.scale = 1
            // return parent item
            dice1.parent = mainFormUnit
            dice2.parent = mainFormUnit
            dice3.parent = mainFormUnit
            dice4.parent = mainFormUnit
            dice5.parent = mainFormUnit
        }

        menuPanel.onPressedUser1: {
            startGameUnit.show()
        }
        menuPanel.onPressedUser2: {
            var winner = currentPlayer===1 ? combinationPanel.nameRow.name2 : combinationPanel.nameRow.name1
            winnerPanel.setWinnerName(winner)
            var score1 = combinationPanel.totalRow.points1
            var score2 = combinationPanel.totalRow.points2
            winnerPanel.setScores(score1,score2)
            winnerPanel.show()
            mainFormUnit.menuPanel.buttonStart.disabled = false
            clearData()

        }
        menuPanel.onPressedUser3: {
            testRules.show()
        }
        menuPanel.onPressedUser4: {
            messageDialog.show("About game","This game created by SyS-team.<br>
                                Game created on QtQuick in the context of project for discipline
                                Software Engineereing. <br>
                                Thank you for using it!")
        }

        //DiceBox Panel
        diceBoxPanel.onStoppedAnim: {
            console.log("diceBoxPanel.onStoppedAnim LAUNCHED")
            dicesFromDicebox()
            //            if(dice1.isInDicebox === true){
            //                dice1.parent = mainFormUnit
            //                dice1.isInDicebox = false
            //                var coord = {x: 0, y: 0}
            //                coord = MyScript.getRandCoords2(mainFormUnit.playField,dice1)
            //                dice1.x = coord.x; dice1.y = coord.y;
            //            }
            //            if(dice2.isInDicebox === true){
            //                dice2.parent = mainFormUnit
            //                dice2.isInDicebox = false
            //                //coord = {x: 0, y: 0}
            //                coord = MyScript.getRandCoords2(mainFormUnit.playField,dice2)
            //                dice2.x = coord.x; dice2.y = coord.y;
            //            }
            //            if(dice3.isInDicebox === true){
            //                dice3.parent = mainFormUnit
            //                dice3.isInDicebox = false
            //                //var coord = {x: 0, y: 0}
            //                coord = MyScript.getRandCoords2(mainFormUnit.playField,dice3)
            //                dice3.x = coord.x; dice3.y = coord.y;
            //            }
            //            if(dice4.isInDicebox === true){
            //                dice4.parent = mainFormUnit
            //                dice4.isInDicebox = false
            //                //var coord = {x: 0, y: 0}
            //                coord = MyScript.getRandCoords2(mainFormUnit.playField,dice4)
            //                dice4.x = coord.x; dice4.y = coord.y;
            //            }
            //            if(dice5.isInDicebox === true){
            //                dice5.parent = mainFormUnit
            //                dice5.isInDicebox = false
            //                //var coord = {x: 0, y: 0}
            //                coord = MyScript.getRandCoords2(mainFormUnit.playField,dice5)
            //                dice5.x = coord.x; dice5.y = coord.y;
            //            }

            mainFormUnit.exDicesInDicebox = 00000
            mainFormUnit.realDicesInDicebox = 00000


        }
        //        diceBoxPanel.onStartedAnim: {
        //            mainFormUnit.diceInDicebox()
        //        }


        function dicesToDicebox(){
            console.log("dicesToDicebox() LAUNCHED")
            //animToDicebox.running = true
            if(dice1.isInStack === false){
                exDicesInDicebox += 10000
                //dice1.isInDicebox = true
                dice1.state = "inDicebox"
            }
            if(dice2.isInStack === false){
                exDicesInDicebox += 1000
                //dice2.isInDicebox = true
                dice2.state = "inDicebox"
            }
            if(dice3.isInStack === false){
                exDicesInDicebox += 100
                //dice3.isInDicebox = true
                dice3.state = "inDicebox"
            }
            if(dice4.isInStack === false){
                exDicesInDicebox += 10
                //dice4.isInDicebox = true
                dice4.state = "inDicebox"
            }
            if(dice5.isInStack === false){
                exDicesInDicebox += 1
                //dice5.isInDicebox = true
                dice5.state = "inDicebox"
            }
        }
        function dicesFromDicebox(){
            console.log("dicesFromDicebox() LAUNCHED")
            DicesData.setDiceValues()

            //animToDicebox.running = true


            //moving from dicebox to field
            if(dice1.state === "inDicebox")dice1.state = "onField"
            if(dice2.state === "inDicebox")dice2.state = "onField"
            if(dice3.state === "inDicebox")dice3.state = "onField"
            if(dice4.state === "inDicebox")dice4.state = "onField"
            if(dice5.state === "inDicebox")dice5.state = "onField"





            diceBoxPanel.decRolls()
            // calc combin for player comfort
            var d1 = dice1.value
            var d2 = dice2.value
            var d3 = dice3.value
            var d4 = dice4.value
            var d5 = dice5.value
            if(currentPlayer===1){
                combinationPanel.combRow1.prePoints1 = CombClass.calcCombination(1,d1,d2,d3,d4,d5)
                combinationPanel.combRow2.prePoints1 = CombClass.calcCombination(2,d1,d2,d3,d4,d5)
                combinationPanel.combRow3.prePoints1 = CombClass.calcCombination(3,d1,d2,d3,d4,d5)
                combinationPanel.combRow4.prePoints1 = CombClass.calcCombination(4,d1,d2,d3,d4,d5)
                combinationPanel.combRow5.prePoints1 = CombClass.calcCombination(5,d1,d2,d3,d4,d5)
                combinationPanel.combRow6.prePoints1 = CombClass.calcCombination(6,d1,d2,d3,d4,d5)
                combinationPanel.combRow7.prePoints1 = CombClass.calcCombination(40,d1,d2,d3,d4,d5)
                combinationPanel.combRow8.prePoints1 = CombClass.calcCombination(23,d1,d2,d3,d4,d5)
                combinationPanel.combRow9.prePoints1 = CombClass.calcCombination(14,d1,d2,d3,d4,d5)
                combinationPanel.combRow10.prePoints1 = CombClass.calcCombination(15,d1,d2,d3,d4,d5)
                combinationPanel.combRow11.prePoints1 = CombClass.calcCombination(50,d1,d2,d3,d4,d5)
                combinationPanel.combRow12.prePoints1 = CombClass.calcCombination(100,d1,d2,d3,d4,d5)

                combinationPanel.combRow1.prePoints2 = 0
                combinationPanel.combRow2.prePoints2 = 0
                combinationPanel.combRow3.prePoints2 = 0
                combinationPanel.combRow4.prePoints2 = 0
                combinationPanel.combRow5.prePoints2 = 0
                combinationPanel.combRow6.prePoints2 = 0
                combinationPanel.combRow7.prePoints2 = 0
                combinationPanel.combRow8.prePoints2 = 0
                combinationPanel.combRow9.prePoints2 = 0
                combinationPanel.combRow10.prePoints2 = 0
                combinationPanel.combRow11.prePoints2 = 0
                combinationPanel.combRow12.prePoints2 = 0
            }
            else{
                combinationPanel.combRow1.prePoints2 = CombClass.calcCombination(1,d1,d2,d3,d4,d5)
                combinationPanel.combRow2.prePoints2 = CombClass.calcCombination(2,d1,d2,d3,d4,d5)
                combinationPanel.combRow3.prePoints2 = CombClass.calcCombination(3,d1,d2,d3,d4,d5)
                combinationPanel.combRow4.prePoints2 = CombClass.calcCombination(4,d1,d2,d3,d4,d5)
                combinationPanel.combRow5.prePoints2 = CombClass.calcCombination(5,d1,d2,d3,d4,d5)
                combinationPanel.combRow6.prePoints2 = CombClass.calcCombination(6,d1,d2,d3,d4,d5)
                combinationPanel.combRow7.prePoints2 = CombClass.calcCombination(40,d1,d2,d3,d4,d5)
                combinationPanel.combRow8.prePoints2 = CombClass.calcCombination(23,d1,d2,d3,d4,d5)
                combinationPanel.combRow9.prePoints2 = CombClass.calcCombination(14,d1,d2,d3,d4,d5)
                combinationPanel.combRow10.prePoints2 = CombClass.calcCombination(15,d1,d2,d3,d4,d5)
                combinationPanel.combRow11.prePoints2 = CombClass.calcCombination(50,d1,d2,d3,d4,d5)
                combinationPanel.combRow12.prePoints2 = CombClass.calcCombination(100,d1,d2,d3,d4,d5)

                combinationPanel.combRow1.prePoints1 = 0
                combinationPanel.combRow2.prePoints1 = 0
                combinationPanel.combRow3.prePoints1 = 0
                combinationPanel.combRow4.prePoints1 = 0
                combinationPanel.combRow5.prePoints1 = 0
                combinationPanel.combRow6.prePoints1 = 0
                combinationPanel.combRow7.prePoints1 = 0
                combinationPanel.combRow8.prePoints1 = 0
                combinationPanel.combRow9.prePoints1 = 0
                combinationPanel.combRow10.prePoints1 = 0
                combinationPanel.combRow11.prePoints1 = 0
                combinationPanel.combRow12.prePoints1 = 0
            }
            // some inits
            var pfX = mainFormUnit.playField.x
            var pfY = mainFormUnit.playField.y
            var coord = {x: 0, y: 0}
            var cells = []
            cells = MyScript.getRandCells3(mainFormUnit.playField,dice1)
            console.log("CELLS: "+cells)
            // and generate cells on playField
            if(dice1.isInStack===false){
                dice1.playFieldCell = cells[0]
            }
            if(dice2.isInStack===false){
                dice2.playFieldCell = cells[1]
            }
            if(dice3.isInStack===false){
                dice3.playFieldCell = cells[2]
            }
            if(dice4.isInStack===false){
                dice4.playFieldCell = cells[3]
            }
            if(dice5.isInStack===false){
                dice5.playFieldCell = cells[4]
            }

        }

        diceBoxPanel.onPressedDiceBox: {
            if(diceBoxPanel.diceBox.rolls===3){
                blockDices(false)
                dice1.moveFromStack(dice1.stackNumber,dice1.isInStack)
                dice2.moveFromStack(dice2.stackNumber,dice2.isInStack)
                dice3.moveFromStack(dice3.stackNumber,dice3.isInStack)
                dice4.moveFromStack(dice4.stackNumber,dice4.isInStack)
                dice5.moveFromStack(dice5.stackNumber,dice5.isInStack)

            }
            //            DicesData.setDiceValues()
            //var pfX = mainFormUnit.playField.x
            //var pfY = mainFormUnit.playField.y
            // generates rand coords and IMMEDIATELY sets it
            //if(dice1.isInStack===false)MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice1)
            //if(dice2.isInStack===false)MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice2)
            //if(dice3.isInStack===false)MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice3)
            //if(dice4.isInStack===false)MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice4)
            //if(dice5.isInStack===false)MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice5)

            //diceInDicebox()
            dicesToDicebox()


        }
        diceBoxPanel.onDecRolls: {
            diceBoxPanel.diceBox.rolls--;
        }
        diceBoxPanel.onResetRolls: {
            diceBoxPanel.diceBox.rolls = 3;
        }
        diceBoxPanel.onRollsChanged: {
            combinationPanel.blockPoints(currentPlayer)
        }

        // CombinationPanel
        combinationPanel.onSetPoints1_1: {

            combinationPanel.combRow1.points1 = combinationPanel.combRow1.prePoints1
            combinationPanel.combRow1.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints1_2: {
            combinationPanel.combRow1.points2 = combinationPanel.combRow1.prePoints2
            combinationPanel.combRow1.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints2_1: {
            combinationPanel.combRow2.points1 = combinationPanel.combRow2.prePoints1
            combinationPanel.combRow2.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints2_2: {
            combinationPanel.combRow2.points2 = combinationPanel.combRow2.prePoints2
            combinationPanel.combRow2.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints3_1: {
            combinationPanel.combRow3.points1 = combinationPanel.combRow3.prePoints1
            combinationPanel.combRow3.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints3_2: {
            combinationPanel.combRow3.points2 = combinationPanel.combRow3.prePoints2
            combinationPanel.combRow3.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints4_1: {
            combinationPanel.combRow4.points1 = combinationPanel.combRow4.prePoints1
            combinationPanel.combRow4.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints4_2: {
            combinationPanel.combRow4.points2 = combinationPanel.combRow4.prePoints2
            combinationPanel.combRow4.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints5_1: {
            combinationPanel.combRow5.points1 = combinationPanel.combRow5.prePoints1
            combinationPanel.combRow5.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints5_2: {
            combinationPanel.combRow5.points2 = combinationPanel.combRow5.prePoints2
            combinationPanel.combRow5.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints6_1: {
            combinationPanel.combRow6.points1 = combinationPanel.combRow6.prePoints1
            combinationPanel.combRow6.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints6_2: {
            combinationPanel.combRow6.points2 = combinationPanel.combRow6.prePoints2
            combinationPanel.combRow6.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints7_1: {
            combinationPanel.combRow7.points1 = combinationPanel.combRow7.prePoints1
            combinationPanel.combRow7.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints7_2: {
            combinationPanel.combRow7.points2 = combinationPanel.combRow7.prePoints2
            combinationPanel.combRow7.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints8_1: {
            combinationPanel.combRow8.points1 = combinationPanel.combRow8.prePoints1
            combinationPanel.combRow8.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints8_2: {
            combinationPanel.combRow8.points2 = combinationPanel.combRow8.prePoints2
            combinationPanel.combRow8.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints9_1: {
            combinationPanel.combRow9.points1 = combinationPanel.combRow9.prePoints1
            combinationPanel.combRow9.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints9_2: {
            combinationPanel.combRow9.points2 = combinationPanel.combRow9.prePoints2
            combinationPanel.combRow9.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints10_1: {
            combinationPanel.combRow10.points1 = combinationPanel.combRow10.prePoints1
            combinationPanel.combRow10.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints10_2: {
            combinationPanel.combRow10.points2 = combinationPanel.combRow10.prePoints2
            combinationPanel.combRow10.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints11_1: {
            combinationPanel.combRow11.points1 = combinationPanel.combRow11.prePoints1
            combinationPanel.combRow11.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints11_2: {
            combinationPanel.combRow11.points2 = combinationPanel.combRow11.prePoints2
            combinationPanel.combRow11.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }
        combinationPanel.onSetPoints12_1: {
            combinationPanel.combRow12.points1 = combinationPanel.combRow12.prePoints1
            combinationPanel.combRow12.points1Entered = true
            combinationPanel.setTotal1()
            diceBoxPanel.resetRolls()
            currentPlayer = 2;
            diceBoxPanel.changePlayer(2);
        }
        combinationPanel.onSetPoints12_2: {
            combinationPanel.combRow12.points2 = combinationPanel.combRow12.prePoints2
            combinationPanel.combRow12.points2Entered = true
            combinationPanel.setTotal2()
            diceBoxPanel.resetRolls()
            closedCombs++;
            if(closedCombs==12)endGame()
            else {
                currentPlayer = 1;
                diceBoxPanel.changePlayer(1);
            }
        }

        combinationPanel.onSetTotal1: {
            var p123456 = combinationPanel.combRow1.points1+
                    combinationPanel.combRow2.points1+
                    combinationPanel.combRow3.points1+
                    combinationPanel.combRow4.points1+
                    combinationPanel.combRow5.points1+
                    combinationPanel.combRow6.points1
            var pGreat = combinationPanel.combRow7.points1+
                    combinationPanel.combRow8.points1+
                    combinationPanel.combRow9.points1+
                    combinationPanel.combRow10.points1+
                    combinationPanel.combRow11.points1+
                    combinationPanel.combRow12.points1
            combinationPanel.totalRow.points1 = p123456+pGreat
        }
        combinationPanel.onSetTotal2: {
            var p123456 = combinationPanel.combRow1.points2+
                    combinationPanel.combRow2.points2+
                    combinationPanel.combRow3.points2+
                    combinationPanel.combRow4.points2+
                    combinationPanel.combRow5.points2+
                    combinationPanel.combRow6.points2
            var pGreat = combinationPanel.combRow7.points2+
                    combinationPanel.combRow8.points2+
                    combinationPanel.combRow9.points2+
                    combinationPanel.combRow10.points2+
                    combinationPanel.combRow11.points2+
                    combinationPanel.combRow12.points2
            combinationPanel.totalRow.points2 = p123456+pGreat
        }


        Dice{
            id: dice1
            z: 5
            objectName: "dice1"
            playFieldParentRef: mainFormUnit.playField
            diceboxParentRef: mainFormUnit.diceBoxPanel.diceBox

            onMoveToStack: {

                //var finded = MyScript.findFreeStack(s1,s2,s3,s4,s5)
                var finded = MyScript.findFreeStack2(mainFormUnit.stackPanel)
                MyScript.moveToStack(dice1, finded, mainFormUnit.stackPanel)
                dice1.state = "inStack"

                //dice1.x = 0; dice1.y = 0;


//                dice1.stackNumber = finded
                // TODO maybe some shorter way to set dice coords?

            }
            //
            onMoveFromStack: {
                MyScript.moveFromStack(dice1,mainFormUnit.stackPanel)

                var free = MyScript.findFreeCellOnField(dice1,dice2,dice3,dice4,dice5)
                dice1.state = "onField"
                dice1.playFieldCell = free

                //coord = MyScript.getRandCoords2(mainFormUnit.playField,dice1)
                //dice1.state = "onField"
                //dice1.x = coord.x; dice1.y = coord.y
                //var pfX = mainFormUnit.playField.x
                //var pfY = mainFormUnit.playField.y
                //var coord = MyScript.getRandCoords(pfX,pfY,mainFormUnit.playField,dice1)
            }
            onDiceArrived: {
                console.log("dice1 arrived to dicebox")
                mainFormUnit.realDicesInDicebox += 10000
            }

        }
        Dice{
            id: dice2
            z: 5
            objectName: "dice2"
            playFieldParentRef: mainFormUnit.playField
            diceboxParentRef: mainFormUnit.diceBoxPanel.diceBox
            onMoveToStack: {
                var finded = MyScript.findFreeStack2(mainFormUnit.stackPanel)
                MyScript.moveToStack(dice2, finded, mainFormUnit.stackPanel)
                dice2.state = "inStack"
                //dice2.x = 0; dice2.y = 0;
            }
            onMoveFromStack: {
                MyScript.moveFromStack(dice2,mainFormUnit.stackPanel)
                var free = MyScript.findFreeCellOnField(dice1,dice2,dice3,dice4,dice5)
                dice2.state = "onField"
                dice2.playFieldCell = free

            }
            onDiceArrived: {
                console.log("dice2 arrived to dicebox")
                mainFormUnit.realDicesInDicebox += 1000
            }
        }
        Dice{
            id: dice3
            z: 5
            objectName: "dice3"
            playFieldParentRef: mainFormUnit.playField
            diceboxParentRef: mainFormUnit.diceBoxPanel.diceBox
            onMoveToStack: {
                var finded = MyScript.findFreeStack2(mainFormUnit.stackPanel)
                MyScript.moveToStack(dice3, finded, mainFormUnit.stackPanel)
                dice3.state = "inStack"
                //dice3.x = 0; dice3.y = 0;
            }
            onMoveFromStack: {
                MyScript.moveFromStack(dice3,mainFormUnit.stackPanel)
                var free = MyScript.findFreeCellOnField(dice1,dice2,dice3,dice4,dice5)
                dice3.state = "onField"
                dice3.playFieldCell = free
            }
            onDiceArrived: {
                console.log("dice3 arrived to dicebox")
                mainFormUnit.realDicesInDicebox += 100
            }
        }
        Dice{
            id: dice4
            z: 5
            objectName: "dice4"
            playFieldParentRef: mainFormUnit.playField
            diceboxParentRef: mainFormUnit.diceBoxPanel.diceBox
            onMoveToStack: {
                var finded = MyScript.findFreeStack2(mainFormUnit.stackPanel)
                MyScript.moveToStack(dice4, finded, mainFormUnit.stackPanel)
                dice4.state = "inStack"
                //dice4.x = 0; dice4.y = 0;
            }
            onMoveFromStack: {
                MyScript.moveFromStack(dice4,mainFormUnit.stackPanel)
                var free = MyScript.findFreeCellOnField(dice1,dice2,dice3,dice4,dice5)
                dice4.state = "onField"
                dice4.playFieldCell = free
            }
            onDiceArrived: {
                console.log("dice4 arrived to dicebox")
                mainFormUnit.realDicesInDicebox += 10
            }
        }
        Dice{
            id: dice5
            z: 5
            objectName: "dice5"
            playFieldParentRef: mainFormUnit.playField
            diceboxParentRef: mainFormUnit.diceBoxPanel.diceBox
            onMoveToStack: {
                var finded = MyScript.findFreeStack2(mainFormUnit.stackPanel)
                MyScript.moveToStack(dice5, finded, mainFormUnit.stackPanel)
                dice5.state = "inStack"
                //dice5.x = 0; dice5.y = 0;
            }
            onMoveFromStack: {
                MyScript.moveFromStack(dice5,mainFormUnit.stackPanel)
                var free = MyScript.findFreeCellOnField(dice1,dice2,dice3,dice4,dice5)
                dice5.state = "onField"
                dice5.playFieldCell = free
            }
            onDiceArrived: {
                console.log("dice5 arrived to dicebox")
                mainFormUnit.realDicesInDicebox += 1
            }
        }
        //
        RulesModal{
            id: testRules
            z: 100
        }
        StartPanel{
            id: startGameUnit
            z: 100
            onApplyClicked: {
                mainFormUnit.combinationPanel.name1 = startGameUnit.playerName1
                mainFormUnit.combinationPanel.name2 = startGameUnit.playerName2
                mainFormUnit.combinationPanel.namesSetted()
                mainFormUnit.startGame()
            }
        }
        WinnerModal{
            id: winnerPanel
            z: 100
        }
        ParallelAnimation{
            id: animToDicebox
            alwaysRunToEnd: true

            property bool start1: false
            property bool start2: false
            property bool start3: false
            property bool start4: false
            property bool start5: false
            onRunningChanged: {
                if(dice1.isInStack === true) dice1ParAnim.start1 = false
                else dice1ParAnim.start1 = true
                if(dice2.isInStack === true) dice2ParAnim.start2 = false
                else dice1ParAnim.start2 = true
                if(dice3.isInStack === true) dice3ParAnim.start3 = false
                else dice1ParAnim.start3 = true
                if(dice4.isInStack === true) dice4ParAnim.start4 = false
                else dice1ParAnim.start4 = true
                if(dice5.isInStack === true) dice5ParAnim.start5 = false
                else dice1ParAnim.start5 = true
            }
            onStopped: {
                mainFormUnit.dicesFromDicebox()
            }

            // TODO create qml filefor animation and change properties of new type when calling
            ParallelAnimation{
                id:dice1ParAnim
                onRunningChanged: (parent.start1 === false) ? stop() : restart()
                NumberAnimation {
                    target: dice1
                    property: "x"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad

                }
                NumberAnimation {
                    target: dice1
                    property: "y"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
            }
            ParallelAnimation{
                id:dice2ParAnim
                NumberAnimation {
                    target: dice2
                    property: "x"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: dice2
                    property: "y"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
            }
            ParallelAnimation{
                id:dice3ParAnim
                NumberAnimation {
                    target: dice3
                    property: "x"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: dice3
                    property: "y"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
            }
            ParallelAnimation{
                id:dice4ParAnim
                NumberAnimation {
                    target: dice4
                    property: "x"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: dice4
                    property: "y"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
            }
            ParallelAnimation{
                id:dice5ParAnim
                NumberAnimation {
                    target: dice5
                    property: "x"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: dice5
                    property: "y"
                    duration: 150
                    to: 10
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }

    //    DicesData{
    //        id: dicesData
    //        dices: [
    //            DiceCpp{
    //                id: diceCpp1
    //            },
    //            DiceCpp{
    //                id: diceCpp2
    //            },
    //            DiceCpp{
    //                id: diceCpp3
    //            },
    //            DiceCpp{
    //                id: diceCpp4
    //            },
    //            DiceCpp{
    //                id: diceCpp5
    //            }
    //        ]
    //    }


    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(title, caption) {
            messageDialog.text = caption;
            messageDialog.title = title;
            messageDialog.open();
        }
    }


    //    Rectangle{
    //        width: 400
    //        height: 400
    //        color: "green"
    //        anchors.bottom: parent.bottom
    //        anchors.right: parent.right

    //        //anchors.right: parent
    //    }

    //    Rectangle {
    //        id: rectangle1

    //        //x: 368
    //        //y: 48
    //        width: 304
    //        height: 576
    //        color: "#f5580a"
    //    }
}
