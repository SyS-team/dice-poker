import QtQuick 2.4

AttentionSignForm {
    id:root
    property bool isActive: false


    signal activate()
    signal deactivate()

    onActivate: {
        root.opacity = 100
        isActive = true
    }
    onDeactivate: {
        root.opacity = 0
        isActive = false
    }


    Behavior on opacity {

        NumberAnimation {
            duration: 200
            easing.type: Easing.InOutQuad
        }
    }
}

