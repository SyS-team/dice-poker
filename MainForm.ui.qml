import QtQuick 2.4

Rectangle {
    id: root

    width: 768
    height: 576

    property alias menuPanel: menuPanel1
    property alias combinationPanel: combinationPanel1
    property alias stackPanel: stackPanel1
    property alias playField: playField1
    property alias actionBar: actionBar1
    property alias diceBoxPanel: diceBoxPanel1
    Image {
        id: image1
        z:0
        anchors.fill: parent
        source: "qrc:/img/img/play_field.png"
    }
    ActionBar {
        id: actionBar1
        anchors.top: root.top
    }

    StackPanel {
        id: stackPanel1
        anchors.top: actionBar1.bottom
        anchors.right: root.right
    }

    CombinationPanel {
        id: combinationPanel1
        anchors.top: actionBar1.bottom
    }

    DiceBoxPanel {
        id: diceBoxPanel1
        anchors.bottom: combinationPanel1.bottom
        anchors.right: combinationPanel1.right
    }

    MenuPanel {
        id: menuPanel1
        anchors.top: combinationPanel1.bottom
    }
    PlayField{
        id: playField1
        anchors.top: stackPanel1.bottom
        anchors.left: combinationPanel1.right
    }

}
