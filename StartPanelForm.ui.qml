import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle {
    id: root
    //anchors.centerIn: parent
    width: 400
    height: 150
    color: "#00000000"

    property alias buttonApply: buttonApply
    property alias enteredName1: textField1.text
    property alias enteredName2: textField2.text
    property alias textField1: textField1
    property alias textField2: textField2
    property alias sign1: attentionSign1
    property alias sign2: attentionSign2
    //signal enterPressed()

    FontLoader { id: fixedFont; source: "qrc:/fonts/Papyrus.ttf" }

    Image {
        id: image1
        anchors.fill: parent
        source: "qrc:/img/img/menu_panel.png"
    }

    Column{
        id: columnPlayerNames
        width: 300
        height: 26
        anchors.verticalCenterOffset: -20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 0
        //anchors.horizontalCenter: parent.horizontalCenter
        Row {
            id:rowPlayer1
            width: parent.width
            Text {
                id: textPlayer1
                width: parent.width/2
                color: "#ffffff"
                text: qsTr("Enter Player 1")
                styleColor: "#008000"
                style: Text.Outline
                font.bold: true
                font.pointSize: 12
                font.family: fixedFont.name

            }

            TextField {
                id: textField1
                width: 127
                height: 25
                font.pointSize: 10
                font.bold: true
                maximumLength: 11
                placeholderText: qsTr("Enter Name 1")
                font.family: fixedFont.name
                //Keys.onEnterPressed: enterPressed()
            }

            AttentionSign {
                id: attentionSign1
                transformOrigin: Item.TopLeft
                scale: 1.4
            }

        }
        Row {
            id: rowPlayer2
            width: parent.width
            Text {
                id: textPlayer2
                width: parent.width/2
                color: "#ffffff"
                text: qsTr("Enter Player 2")
                style: Text.Outline
                styleColor: "#008000"
                font.bold: true
                font.pointSize: 12
                font.family: fixedFont.name
            }


            TextField {
                id: textField2
                width: 127
                height: 25
                font.pointSize: 10
                maximumLength: 11
                font.bold: true
                placeholderText: qsTr("Enter Name 2")
                font.family: fixedFont.name
            }

            AttentionSign {
                id: attentionSign2
                scale: 1.4
                transformOrigin: Item.TopLeft
            }
        }
    }
    MenuButton{
        id: buttonApply
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: 100
        height: 35
        text: qsTr("Apply")
        anchors.bottomMargin: 15
        anchors.rightMargin: 20
        anchors.margins: 10

    }

    // I don't remember what for i added this Behavior below

//    Behavior on x{

//        NumberAnimation {
//            duration: 300
//            easing.type: Easing.OutQuad
//        }
//    }
}


