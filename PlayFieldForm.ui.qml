import QtQuick 2.4

Rectangle {
    id: root
    width: 464
    height: 432
    color: "transparent"

    Rectangle {
        id: rectangle1
        width: parent.width/5
        height: parent.height/5
        color: "#f62020"
        visible: false
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
    }

    Rectangle {
        id: rectangle2
        width: parent.width/5
        height: parent.height/5
        color: "#ece005"
        visible: false
        anchors.left: rectangle1.right
        anchors.top: parent.top
    }

    Rectangle {
        id: rectangle3
        width: parent.width/5
        height: parent.height/5
        color: "#3bf117"
        visible: false
        anchors.left: rectangle2.right
        anchors.top: parent.top
    }

    Rectangle {
        id: rectangle4
        width: parent.width/5
        height: parent.height/5
        color: "#0ffdbb"
        visible: false
        anchors.left: rectangle3.right
        anchors.top: parent.top
    }

    Rectangle {
        id: rectangle5
        x: -7
        width: 92.8
        height: parent.height/5
        color: "#0510db"
        visible: false
        anchors.right: parent.right
        anchors.top: parent.top
    }

    Rectangle {
        id: rectangle6
        width: parent.width/5
        height: parent.height/5
        color: "#fa1cfa"
        visible: false
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.top: rectangle1.bottom
        anchors.left: parent.left
    }

    Rectangle {
        id: rectangle7
        width: parent.width/5
        height: parent.height/5
        color: "#f9910f"
        visible: false
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.left: rectangle6.right
        anchors.top: rectangle2.bottom
    }

    Rectangle {
        id: rectangle8
        width: parent.width/5
        height: parent.height/5
        color: "#baef03"
        visible: false
        anchors.left: rectangle7.right
        anchors.top: rectangle3.bottom
    }

    Rectangle {
        id: rectangle9
        width: parent.width/5
        height: parent.height/5
        color: "#07fc07"
        visible: false
        anchors.left: rectangle8.right
        anchors.top: rectangle4.bottom
    }

    Rectangle {
        id: rectangle10
        width: parent.width/5
        height: parent.height/5
        color: "#11e995"
        visible: false
        anchors.left: rectangle9.right
        anchors.top: rectangle5.bottom
    }

    Rectangle {
        id: rectangle11
        width: parent.width/5
        height: parent.height/5
        color: "#26caf8"
        visible: false
        anchors.top: rectangle6.bottom
        anchors.left: parent.left
    }

    Rectangle {
        id: rectangle12
        width: parent.width/5
        height: parent.height/5
        color: "#0711b8"
        visible: false
        anchors.left: rectangle11.right
        anchors.top: rectangle7.bottom
    }

    Rectangle {
        id: rectangle13
        width: parent.width/5
        height: parent.height/5
        color: "#b816f7"
        visible: false
        anchors.left: rectangle12.right
        anchors.top: rectangle8.bottom
    }

    Rectangle {
        id: rectangle14
        width: parent.width/5
        height: parent.height/5
        color: "#fb89d5"
        visible: false
        anchors.left: rectangle13.right
        anchors.top: rectangle9.bottom
    }

    Rectangle {
        id: rectangle15
        x: 2
        width: parent.width/5
        height: parent.height/5
        color: "#f72208"
        visible: false
        anchors.top: rectangle10.bottom
        anchors.right: parent.right
    }

    Rectangle {
        id: rectangle16
        width: parent.width/5
        height: parent.height/5
        color: "#f7b768"
        visible: false
        anchors.topMargin: 0
        anchors.top: rectangle11.bottom
        anchors.left: parent.left
    }

    Rectangle {
        id: rectangle17
        width: parent.width/5
        height: parent.height/5
        color: "#b2e62b"
        visible: false
        anchors.top: rectangle12.bottom
        anchors.left: rectangle16.right
    }

    Rectangle {
        id: rectangle18
        width: parent.width/5
        height: parent.height/5
        color: "#0be60b"
        visible: false
        anchors.left: rectangle17.right
        anchors.top: rectangle13.bottom
    }

    Rectangle {
        id: rectangle19
        width: parent.width/5
        height: parent.height/5
        color: "#18f5c4"
        visible: false
        anchors.left: rectangle18.right
        anchors.top: rectangle14.bottom
    }

    Rectangle {
        id: rectangle20
        x: 4
        width: 92.8
        height: parent.height/5
        color: "#0e36c4"
        visible: false
        anchors.right: parent.right
        anchors.top: rectangle15.bottom
    }

    Rectangle {
        id: rectangle21
        y: -2
        width: parent.width/5
        height: parent.height/5
        color: "#960eda"
        visible: false
        anchors.bottom: parent.bottom
        anchors.left: parent.left
    }

    Rectangle {
        id: rectangle22
        y: 1
        width: parent.width/5
        height: parent.height/5
        color: "#e60707"
        visible: false
        anchors.left: rectangle21.right
        anchors.bottom: parent.bottom
    }

    Rectangle {
        id: rectangle23
        y: 5
        width: parent.width/5
        height: parent.height/5
        color: "#fbfb1b"
        visible: false
        anchors.left: rectangle22.right
        anchors.bottom: parent.bottom
    }

    Rectangle {
        id: rectangle24
        y: 0
        width: parent.width/5
        height: parent.height/5
        color: "#0dfb42"
        visible: false
        anchors.left: rectangle23.right
        anchors.bottom: parent.bottom
    }

    Rectangle {
        id: rectangle25
        x: -4
        y: -3
        width: parent.width/5
        height: parent.height/5
        color: "#ebf80c"
        visible: false
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
    //color: "darkgreen"


}

