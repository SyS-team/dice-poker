import QtQuick 2.4

ModalWindow{
    id: root
    closeable: true

    function setWinnerName(winner){
        winnerModalForm.winnerName = winner
    }
    function setScores(score1,score2){
        winnerModalForm.score1 = score1
        winnerModalForm.score2 = score2
    }
    function gameDraw(){
        winnerModalForm.textWin.visible = false
        winnerModalForm.textDraw.visible = true
    }

    WinnerModalForm {
        id: winnerModalForm
        anchors.centerIn: parent
    }
}
