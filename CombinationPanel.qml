import QtQuick 2.4

CombinationPanelForm {
    id: root


    property string name1
    property string name2


    signal namesSetted()
    signal setPoints1_1()
    signal setPoints1_2()
    signal setPoints2_1()
    signal setPoints2_2()
    signal setPoints3_1()
    signal setPoints3_2()
    signal setPoints4_1()
    signal setPoints4_2()
    signal setPoints5_1()
    signal setPoints5_2()
    signal setPoints6_1()
    signal setPoints6_2()
    signal setPoints7_1()
    signal setPoints7_2()
    signal setPoints8_1()
    signal setPoints8_2()
    signal setPoints9_1()
    signal setPoints9_2()
    signal setPoints10_1()
    signal setPoints10_2()
    signal setPoints11_1()
    signal setPoints11_2()
    signal setPoints12_1()
    signal setPoints12_2()

    signal setTotal1()
    signal setTotal2()
    //signal blockPoints1()
    //signal blockPoints2()

    function blockAllPoints(){
        combRow1.textPoints1.enabled = false
        combRow2.textPoints1.enabled = false
        combRow3.textPoints1.enabled = false
        combRow4.textPoints1.enabled = false
        combRow5.textPoints1.enabled = false
        combRow6.textPoints1.enabled = false
        combRow7.textPoints1.enabled = false
        combRow8.textPoints1.enabled = false
        combRow9.textPoints1.enabled = false
        combRow10.textPoints1.enabled = false
        combRow11.textPoints1.enabled = false
        combRow12.textPoints1.enabled = false

        combRow1.textPoints2.enabled = false
        combRow2.textPoints2.enabled = false
        combRow3.textPoints2.enabled = false
        combRow4.textPoints2.enabled = false
        combRow5.textPoints2.enabled = false
        combRow6.textPoints2.enabled = false
        combRow7.textPoints2.enabled = false
        combRow8.textPoints2.enabled = false
        combRow9.textPoints2.enabled = false
        combRow10.textPoints2.enabled = false
        combRow11.textPoints2.enabled = false
        combRow12.textPoints2.enabled = false
    }

    function blockPoints(currentPlayer){
        if(currentPlayer===1){
            combRow1.textPoints1.enabled = true
            combRow2.textPoints1.enabled = true
            combRow3.textPoints1.enabled = true
            combRow4.textPoints1.enabled = true
            combRow5.textPoints1.enabled = true
            combRow6.textPoints1.enabled = true
            combRow7.textPoints1.enabled = true
            combRow8.textPoints1.enabled = true
            combRow9.textPoints1.enabled = true
            combRow10.textPoints1.enabled = true
            combRow11.textPoints1.enabled = true
            combRow12.textPoints1.enabled = true

            combRow1.textPoints2.enabled = false
            combRow2.textPoints2.enabled = false
            combRow3.textPoints2.enabled = false
            combRow4.textPoints2.enabled = false
            combRow5.textPoints2.enabled = false
            combRow6.textPoints2.enabled = false
            combRow7.textPoints2.enabled = false
            combRow8.textPoints2.enabled = false
            combRow9.textPoints2.enabled = false
            combRow10.textPoints2.enabled = false
            combRow11.textPoints2.enabled = false
            combRow12.textPoints2.enabled = false
        }
        else {
            combRow1.textPoints1.enabled = false
            combRow2.textPoints1.enabled = false
            combRow3.textPoints1.enabled = false
            combRow4.textPoints1.enabled = false
            combRow5.textPoints1.enabled = false
            combRow6.textPoints1.enabled = false
            combRow7.textPoints1.enabled = false
            combRow8.textPoints1.enabled = false
            combRow9.textPoints1.enabled = false
            combRow10.textPoints1.enabled = false
            combRow11.textPoints1.enabled = false
            combRow12.textPoints1.enabled = false

            combRow1.textPoints2.enabled = true
            combRow2.textPoints2.enabled = true
            combRow3.textPoints2.enabled = true
            combRow4.textPoints2.enabled = true
            combRow5.textPoints2.enabled = true
            combRow6.textPoints2.enabled = true
            combRow7.textPoints2.enabled = true
            combRow8.textPoints2.enabled = true
            combRow9.textPoints2.enabled = true
            combRow10.textPoints2.enabled = true
            combRow11.textPoints2.enabled = true
            combRow12.textPoints2.enabled = true
        }
    }

    function resetData(){ // reset scores to start new game
        combRow1.resetPoints()
        combRow2.resetPoints()
        combRow3.resetPoints()
        combRow4.resetPoints()
        combRow5.resetPoints()
        combRow6.resetPoints()
        combRow7.resetPoints()
        combRow8.resetPoints()
        combRow9.resetPoints()
        combRow10.resetPoints()
        combRow11.resetPoints()
        combRow12.resetPoints()
        totalRow.resetPoints()
    }

//    combRow1.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow1.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow2.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow2.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow3.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow3.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow4.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow4.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow5.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow5.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow6.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow6.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow7.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow7.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow8.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow8.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow9.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow9.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow10.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow10.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow11.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow11.onBlockPoints2: {
//        blockPoints2()
//    }
//    combRow12.onBlockPoints1: {
//        blockPoints1()
//    }
//    combRow12.onBlockPoints2: {
//        blockPoints2()
//    }


    combRow1.onSetPoints1:{
        setPoints1_1()
    }
    combRow1.onSetPoints2:{
        setPoints1_2()
    }
    combRow2.onSetPoints1:{
        setPoints2_1()
    }
    combRow2.onSetPoints2:{
        setPoints2_2()
    }
    combRow3.onSetPoints1:{
        setPoints3_1()
    }
    combRow3.onSetPoints2:{
        setPoints3_2()
    }
    combRow4.onSetPoints1:{
        setPoints4_1()
    }
    combRow4.onSetPoints2:{
        setPoints4_2()
    }
    combRow5.onSetPoints1:{
        setPoints5_1()
    }
    combRow5.onSetPoints2:{
        setPoints5_2()
    }
    combRow6.onSetPoints1:{
        setPoints6_1()
    }
    combRow6.onSetPoints2:{
        setPoints6_2()
    }
    combRow7.onSetPoints1:{
        setPoints7_1()
    }
    combRow7.onSetPoints2:{
        setPoints7_2()
    }
    combRow8.onSetPoints1:{
        setPoints8_1()
    }
    combRow8.onSetPoints2:{
        setPoints8_2()
    }
    combRow9.onSetPoints1:{
        setPoints9_1()
    }
    combRow9.onSetPoints2:{
        setPoints9_2()
    }
    combRow10.onSetPoints1:{
        setPoints10_1()
    }
    combRow10.onSetPoints2:{
        setPoints10_2()
    }
    combRow11.onSetPoints1:{
        setPoints11_1()
    }
    combRow11.onSetPoints2:{
        setPoints11_2()
    }
    combRow12.onSetPoints1:{
        setPoints12_1()
    }
    combRow12.onSetPoints2:{
        setPoints12_2()
    }

    totalRow.onSetTotal1: {
        setTotal1()
    }
    totalRow.onSetTotal2: {
        setTotal2()
    }

    onNamesSetted: nameRow.setNames(name1, name2)


}

