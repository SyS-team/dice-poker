import QtQuick 2.4

TotalRowForm {
    id: root
    signal setTotal1()
    signal setTotal2()

    function resetPoints(){
        root.points1 = 0
        root.points2 = 0
    }
}

