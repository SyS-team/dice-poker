import QtQuick 2.4
//import com.ss.dice 1.0
import "MyInterface.js" as MyScript
// проще через svg
// убрать все нах
Rectangle {
    id: dice
    width: 72
    height: 72
    color: "transparent"
    border.color: "transparent" //TO DELETE. have dependencies
    radius: 5
    state: "onField"

    property string diceState
    property int stackNumber: 0         // number of stack in stackPanel
    property int playFieldCell: -1      // number of cell on playField in which generates "random" coords
    property bool isInStack: false
    property int value: 6               // dice points
    property bool isInDicebox: false
    property Item diceboxParentRef
    property Item playFieldParentRef
    property Item freeStackRef

    signal freeFound(int free)
    signal moveToStack(int stackNumber, bool isInStack)
    signal moveFromStack(int stackNumber, bool isInStack)
    signal moveFromStackToDicebox() // dont used??
    signal stoppedMoving()
    signal diceArrived()


    function setDiceToFreeCell (free){
        //var free = MyScript.findFreeCellOnField(d1,d2,d3,d4,d5)
        dice.state = "onField"
        dice.playFieldCell = free
    }

    onPlayFieldCellChanged: {
        console.log("onPlayFieldCellChanged() RUN")
        if(dice.playFieldCell !== -1){
            console.log("onPlayFieldCellChanged() START")
            var coord = {x:0, y:0}
            coord = MyScript.getRandCoordInCell(dice.playFieldCell,playFieldParentRef,dice)
            dice.x = coord.x; dice.y = coord.y
        }
        else console.log("onPlayFieldCellChanged() START FAILED:"+dice.playFieldCell+";"+dice.isInStack+";"+dice.isInDicebox)
    }

    onValueChanged: {
        switch(dice.value){
        case 1: dice.diceState = "one"; break
        case 2: dice.diceState = "two"; break
        case 3: dice.diceState = "three"; break
        case 4: dice.diceState = "four"; break
        case 5: dice.diceState = "five"; break
        case 6: dice.diceState = "six"; break
        }
    }
    onStoppedMoving: {
        console.log("Dice.stoppedMoving() RUN")
        if(dice.isInDicebox === true) diceArrived()
    }

    states: [
        State {
            name: "onField"
            PropertyChanges {
                target: dice
                isInStack: false
                isInDicebox: false
                scale: 1
                z: 5
                opacity: 1
            }
            ParentChange{
                target: dice
                parent: playFieldParentRef
            }

        },
        State {
            name: "inDicebox"
            ParentChange{
                target: dice
                parent: diceboxParentRef
            }
            PropertyChanges {
                target: dice
                isInDicebox: true
                isInStack: false
                x: 0
                y: 0
                z: 2
                scale: 0.2
                opacity: 0
            }


        },
        State {
            name: "inStack"

            PropertyChanges {
                target: dice
                isInStack: true
                //x:100
                //y:100
            }
            ParentChange {
                target: dice
                parent: freeStackRef
            }

        }
    ]
    transitions: [
        Transition {
            from: "onField"
            to: "inDicebox"
            onRunningChanged:{
                if(running===false){
                    console.log("Transition.running = false")
                    stoppedMoving()
                }
            }

            ParallelAnimation{
                id: parallelAnimCoords
                NumberAnimation {
                    target: dice
                    property: "x"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: dice
                    property: "y"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: dice
                    property: "scale"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                NumberAnimation {
                    target: dice
                    property: "opacity"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
            }
        },
        Transition {
            from: "inDicebox"
            to: "onField"
            animations: parallelAnimCoords
        },
        Transition {
            from: "inStack"
            to: "onField"
            animations: parallelAnimCoords
        },
        Transition {
            from: "onField"
            to: "inStack"
            animations: parallelAnimCoords
        },
        Transition {
            from: "inStack"
            to: "inDicebox"
            animations: parallelAnimCoords
            onRunningChanged:{
                if(running===false){
                    console.log("Transition.running = false")
                    stoppedMoving()
                }
            }

        }
    ]

    Rectangle{
        id: redRect
        color: "black"
        anchors.centerIn: parent
        width: 48
        height: 48

    }


    Image{
        id: diceImage
        //width: parent.width
        //height: parent.height
        source: "/img/img/dice_icons/dice_6.png"
        //source:
        anchors.fill: parent
        state: diceState
        states:[
            State{
                name: "one"
                PropertyChanges {
                    target: diceImage
                    source: "/img/img/dice_icons/dice_1.png"

                }
            },
            State{
                name: "two"
                PropertyChanges {
                    target: diceImage
                    source: "/img/img/dice_icons/dice_2.png"
                }
            },
            State{
                name: "three"
                PropertyChanges {
                    target: diceImage
                    source: "/img/img/dice_icons/dice_3.png"
                }
            },
            State{
                name: "four"
                PropertyChanges {
                    target: diceImage
                    source: "/img/img/dice_icons/dice_4.png"
                }
            },
            State{
                name: "five"
                PropertyChanges {
                    target: diceImage
                    source: "/img/img/dice_icons/dice_5.png"
                }
            },
            State{
                name: "six"
                PropertyChanges {
                    target: diceImage
                    source: "/img/img/dice_icons/dice_6.png"
                }
            }

        ]

    }
    MouseArea{
        id:diceMouseArea
        anchors.fill: parent
        onClicked: {
            var free
            freeFound(free)
            var iis = MyScript.checkDice(dice.isInStack)
            //if(iis === 1) MyScript.moveToStack(dice.isInStack,dice.stackNumber,free)
            if(iis===0)moveToStack(stackNumber,isInStack)
            else moveFromStack(stackNumber,isInStack)

            //checkStack(stackUnit);
            //console.log(stackUnit.x+";"+stackUnit.y)
            //moveToStack(s,stackUnit);
        }
    }
    Behavior on x{
        //console.log("wdw")
        NumberAnimation {
            duration: 200
            easing.type: Easing.InOutQuad
            onStopped: {
                console.log("Dice.BehaviorOnX.NumberAnimation.stopped() RUN")
                //stoppedMoving()
                console.log(dice.id+"(dice) X moving stopped")
            }
        }
    }
    Behavior on y{

        NumberAnimation {
            duration: 200
            easing.type: Easing.InOutQuad
        }
    }
    Behavior on scale {
        NumberAnimation {
            duration: 150
            easing.type: Easing.InOutQuad
        }

    }

    function checkStack(stackUnit){ // NOT USED
        // move from stack
        if(dice.isInStack){
            //console.log(dice.stackNumber)
            switch(dice.stackNumber){
            case 1:{
                stackUnit.firstStack = false
                dice.stackNumber = 0
                dice.x = 300
                dice.y = 200
                dice.isInStack = false
                break
            }
            case 2:{
                //console.log("case2")
                stackUnit.secondStack = false
                dice.stackNumber = 0
                dice.x = 400
                dice.y = 200
                dice.isInStack = false
                break
            }
            case 3:{
                stackUnit.thirdStack = false
                dice.stackNumber = 0
                dice.x = 500
                dice.y = 200
                dice.isInStack = false
                break
            }
            case 4:{
                stackUnit.foursStack = false
                dice.stackNumber = 0
                dice.x = 600
                dice.y = 200
                dice.isInStack = false
                break
            }
            case 5:{
                stackUnit.fivesStack = false
                dice.stackNumber = 0
                dice.x = 500
                dice.y = 300
                dice.isInStack = false
                break
            }
            }

            return 0
        }
        //check free stack
        var s = 0
        if(stackUnit.firstStack===false){
            stackUnit.firstStack=true
            dice.isInStack = true

            //a=true;
            s= 1
        }
        else if(stackUnit.secondStack===false){
            stackUnit.secondStack=true
            dice.isInStack = true
//            b=true;
            s= 2
        }
        else if(stackUnit.thirdStack===false){
            stackUnit.thirdStack=true
            dice.isInStack = true
//            c=true
            s= 3
        }
        else if(stackUnit.foursStack===false){
            stackUnit.foursStack=true
            dice.isInStack = true
//            d=true
            s= 4
        }
        else if(stackUnit.fivesStack===false){
            stackUnit.fivesStack=true
            dice.isInStack = true
//            e=true
            s= 5
        }
        //move to stack
        switch(s){
        case 1:{
            dice.x = stackUnit.firstX
            dice.y = stackUnit.stackY
            dice.stackNumber = 1
            break
        }
        case 2:{
            dice.x = stackUnit.secondX
            dice.y = stackUnit.stackY
            dice.stackNumber = 2
            break
        }
        case 3:{
            dice.x = stackUnit.thirdX
            dice.y = stackUnit.stackY
            dice.stackNumber = 3
            break
        }
        case 4:{
            dice.x = stackUnit.foursX
            dice.y = stackUnit.stackY
            dice.stackNumber = 4
            break
        }
        case 5:{
            dice.x = stackUnit.fivesX
            dice.y = stackUnit.stackY
            dice.stackNumber = 5
            break
        }
        }

    }


}

