#include "combination.h"

Combination::Combination()
{

}

Combination::~Combination()
{

}

void Combination::setName(QString name)
{
    this->m_name = name;
}

QString Combination::getName()
{
    return this->m_name;
}

void Combination::setPoints(int points)
{
    this->m_points = points;
}

int Combination::getPoints()
{
    return this->m_points;
}

int Combination::calcCombination(int comb, int dice1, int dice2, int dice3, int dice4, int dice5)
{
    //int result = 0;
    switch (comb) {
    case ones:{
        return calcOnes(dice1,dice2,dice3,dice4,dice5);
    }break;
    case twos:{
        return calcTwos(dice1,dice2,dice3,dice4,dice5);
    }break;
    case threes:{
        return calcThrees(dice1,dice2,dice3,dice4,dice5);
    }break;
    case fours:{
        return calcFours(dice1,dice2,dice3,dice4,dice5);
    }break;
    case fives:{
        return calcFives(dice1,dice2,dice3,dice4,dice5);
    }break;
    case sixes:{
        return calcSixes(dice1,dice2,dice3,dice4,dice5);
    }break;
    case fok:{
        return calcFourOfKind(dice1,dice2,dice3,dice4,dice5);
    }break;
    case full:{
        return calcFullHouse(dice1,dice2,dice3,dice4,dice5);
    }break;
    case small:{
        return calcSmallStraight(dice1,dice2,dice3,dice4,dice5);
    }break;
    case large:{
        return calcLargeStraight(dice1,dice2,dice3,dice4,dice5);
    }break;
    case poker:{
        return calcPoker(dice1,dice2,dice3,dice4,dice5);
    }break;
    case chance:{
        return calcChance(dice1,dice2,dice3,dice4,dice5);
    }break;
    default:
        break;
    }
    return -1;
}

int Combination::calcOnes(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    if(d1==1) result++;
    if(d2==1) result++;
    if(d3==1) result++;
    if(d4==1) result++;
    if(d5==1) result++;
    return result;

}
int Combination::calcTwos(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    if(d1==2) result+=2;
    if(d2==2) result+=2;
    if(d3==2) result+=2;
    if(d4==2) result+=2;
    if(d5==2) result+=2;
    return result;

}
int Combination::calcThrees(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    if(d1==3) result+=3;
    if(d2==3) result+=3;
    if(d3==3) result+=3;
    if(d4==3) result+=3;
    if(d5==3) result+=3;
    return result;

}
int Combination::calcFours(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    if(d1==4) result+=4;
    if(d2==4) result+=4;
    if(d3==4) result+=4;
    if(d4==4) result+=4;
    if(d5==4) result+=4;
    return result;

}
int Combination::calcFives(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    if(d1==5) result+=5;
    if(d2==5) result+=5;
    if(d3==5) result+=5;
    if(d4==5) result+=5;
    if(d5==5) result+=5;
    return result;

}
int Combination::calcSixes(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    if(d1==6) result+=6;
    if(d2==6) result+=6;
    if(d3==6) result+=6;
    if(d4==6) result+=6;
    if(d5==6) result+=6;
    return result;

}
//int Combination::calcThreeOfKind(int d1, int d2, int d3, int d4, int d5){
//    int result = 0;
//    uint a_res [6] = {0,0,0,0,0,0};

//    a_res[d1-1]++;
//    a_res[d2-1]++;
//    a_res[d3-1]++;
//    a_res[d4-1]++;
//    a_res[d5-1]++;

//    for(int i=0;i<6;i++){
//        if(a_res[i]>=3)return (d1+d2+d3+d4+d5);
//    }
//    return result;

//}
int Combination::calcFourOfKind(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    uint a_res [6] = {0,0,0,0,0,0};

    a_res[d1-1]++;
    a_res[d2-1]++;
    a_res[d3-1]++;
    a_res[d4-1]++;
    a_res[d5-1]++;

    for(int i=0;i<6;i++){
        if(a_res[i]>=4)return (d1+d2+d3+d4+d5);
    }
    return result;

}
int Combination::calcFullHouse(int d1, int d2, int d3, int d4, int d5){
    //int result = 0;
    uint a_res [6] = {0,0,0,0,0,0};

    a_res[d1-1]++;
    a_res[d2-1]++;
    a_res[d3-1]++;
    a_res[d4-1]++;
    a_res[d5-1]++;

    bool two=0,three=0;

    for(int i=0;i<6;i++){
        if(a_res[i]==2)two=1;
        else if(a_res[i]==3)three=1;
    }
    if(two && three)return 25;
    else return 0;
    return -1;
//    return result;

}
int Combination::calcSmallStraight(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    uint a_res [6] = {0,0,0,0,0,0};

    a_res[d1-1]++;
    a_res[d2-1]++;
    a_res[d3-1]++;
    a_res[d4-1]++;
    a_res[d5-1]++;

    if(a_res[1]!=0 && a_res[2]!=0 && a_res[3]!=0 && a_res[4]!=0)return 30;
    if(a_res[0]!=0 && a_res[1]!=0 && a_res[2]!=0 && a_res[3]!=0)return 30;
    if(a_res[2]!=0 && a_res[3]!=0 && a_res[4]!=0 && a_res[5]!=0)return 30;
    return result;

}
int Combination::calcLargeStraight(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    uint a_res [6] = {0,0,0,0,0,0};

    a_res[d1-1]++;
    a_res[d2-1]++;
    a_res[d3-1]++;
    a_res[d4-1]++;
    a_res[d5-1]++;

    if(a_res[1]!=0 && a_res[2]!=0 && a_res[3]!=0 && a_res[4]!=0 && a_res[5]!=0)return 40;
    if(a_res[0]!=0 && a_res[1]!=0 && a_res[2]!=0 && a_res[3]!=0 && a_res[4]!=0)return 40;
    return result;

}

int Combination::calcPoker(int d1, int d2, int d3, int d4, int d5){
    int result = 0;
    uint a_res [6] = {0,0,0,0,0,0};

    a_res[d1-1]++;
    a_res[d2-1]++;
    a_res[d3-1]++;
    a_res[d4-1]++;
    a_res[d5-1]++;

    for(int i=0;i<6;i++){
        if(a_res[i]==5)return 50;
    }
    return result;

}
int Combination::calcChance(int d1, int d2, int d3, int d4, int d5){
    return (d1+d2+d3+d4+d5);
}
