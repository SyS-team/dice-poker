import QtQuick 2.4

MenuPanelForm {
    id: root

    signal pressedUser1()
    signal pressedUser2()
    signal pressedUser3()
    signal pressedUser4()
    signal pressedUser5()

    buttonStart.onPressedUser: pressedUser1()
    buttonGiveup.onPressedUser: pressedUser2()
    buttonRules.onPressedUser: pressedUser3()
    buttonAbout.onPressedUser: pressedUser4()
    buttonPreferences.onPressedUser: pressedUser5()
}

