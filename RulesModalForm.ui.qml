import QtQuick 2.4
// doesn't display in design mode because this s*it modified by ModalWindow.qml

ModalWindow {
    id: root

    //width: 700
    //height: 700

    property alias text: rulesText.text
    property alias backImageSource: rulesBackImage.source

    Rectangle {
        id:rulesPanel
        anchors.centerIn: parent
        width: 600
        height: 500
        color: "transparent"
        //x: parent.width//+width
        //y: parent.height - height
        BorderImage {
            id: rulesBackImage
            source: "img/comb_panel.png"
            anchors.fill: parent
            border.left: 5; border.top: 5
            border.right: 5; border.bottom: 5
            Text{
                id: rulesText
                wrapMode: Text.Wrap
                font.pointSize: 11
                text:"<h3><center><b>THE RULES</b></center></h3><br>"+
                     "This is game named Yahtzee."+
                     "In russian it's known as 'Dice Poker' or 'Dices'<br>"+
                     "In the upper section there are six boxes. The score in each of these boxes is determined by adding the total number of dice matching that box.<br>"+
                     "**-**-**-**-**-**-**-**<br>"+
                     "Ones:   1-1-1-3-3 Scores: 3<br>"+
                     "Twos:   2-2-5-1-6 Scores: 4<br>"+
                     "Threes: 3-5-4-6-3 Scores: 6<br>"+
                     "Fours:  6-5-2-6-6 Scores: 0<br>"+
                     "Fives:  5-5-5-5-3 Scores: 25<br>"+
                     "Sixes:  6-1-2-3-6 Scores: 12<br>"+
                     "**-**-**-**-**-**-**-**<br>"+
                     "The lower section contains a number of poker-themed categories with specific point values.<br>"+
                     "For Four-Of-A-Kind and Chance player scores sum of all dices and for other combinations constant value<br>"+
                     "**-**-**-**-**-**-**-**<br>"+
                     "Four-Of-A-Kind(At least four dice the same):              1-1-1-1-3 Scores: 7<br>"+
                     "Full House(Three of one number and two of another):       2-2-5-2-5 Scores: 25<br>"+
                     "Small Straight(Four sequential dice):                     4-1-2-3-2 Scores: 30<br>"+
                     "Large Straight(Five sequential dice):                     6-3-2-5-4 Scores: 40<br>"+
                     "Poker(All five dice the same):                            2-2-2-2-2 Scores: 50<br>"+
                     "Chance(Any combination):                                  6-1-2-3-6 Scores: 18<br>"
                anchors.left: parent.left;anchors.right: parent.right;anchors.top: parent.top;anchors.bottom: parent.bottom
                anchors.leftMargin: 16
                anchors.rightMargin: 16
                anchors.topMargin: 16
                anchors.bottomMargin: 16
            }
        }
    }
}

