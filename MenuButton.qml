import QtQuick 2.4

MenuButtonForm {
    id: root
    //opacity: 0.88

    property bool disabled: false
    onDisabledChanged: {
        if(root.disabled===true){
            root.opacity = 0.88
            root.color = "gray"
        }
        (root.disabled===true) ? mouseAreaButton.enabled = false : mouseAreaButton.enabled = true

    }


    signal pressedUser()

    mouseAreaButton.onEntered: {
        root.color = "green"
    }
    mouseAreaButton.onExited: {
        root.color = "darkgreen"
    }
    mouseAreaButton.onReleased: {
        root.scale = 1
    }
    mouseAreaButton.onPressed: {
        root.scale = 0.95
        pressedUser()
    }

    Behavior on scale{

        NumberAnimation {
            duration: 75
            easing.type: Easing.InOutQuad
        }
    }
}

