#ifndef COMBINATION_H
#define COMBINATION_H

#include <QQuickItem>
#include <QString>

class Combination : public QQuickItem
{
    Q_OBJECT
private:
    enum eComb{
        ones = 1,
        twos = 2,
        threes= 3,
        fours = 4,
        fives = 5,
        sixes = 6,
        fok = 40,
        full = 23,
        small = 14,
        large = 15,
        poker = 50,
        chance = 100
    };
    QString m_name;
    int m_points;
    eComb m_comb;
public:
    Combination();

//    Combination(eComb e_comb){

//    }

    ~Combination();



    void setName(QString name);
    QString getName();
    void setPoints(int points);
    int getPoints();
    Q_INVOKABLE int calcCombination(int comb,int dice1, int dice2, int dice3, int dice4, int dice5);

    int calcLargeStraight(int d1, int d2, int d3, int d4, int d5);
    int calcSmallStraight(int d1, int d2, int d3, int d4, int d5);
    int calcFullHouse(int d1, int d2, int d3, int d4, int d5);
    int calcFourOfKind(int d1, int d2, int d3, int d4, int d5);
    //int calcThreeOfKind(int d1, int d2, int d3, int d4, int d5);
    int calcSixes(int d1, int d2, int d3, int d4, int d5);
    int calcFives(int d1, int d2, int d3, int d4, int d5);
    int calcFours(int d1, int d2, int d3, int d4, int d5);
    int calcThrees(int d1, int d2, int d3, int d4, int d5);
    int calcTwos(int d1, int d2, int d3, int d4, int d5);
    int calcOnes(int d1, int d2, int d3, int d4, int d5);
    int calcPoker(int d1, int d2, int d3, int d4, int d5);
    int calcChance(int d1, int d2, int d3, int d4, int d5);

signals:

public slots:
};

#endif // COMBINATION_H
