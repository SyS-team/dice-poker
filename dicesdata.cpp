///////////////////////////////////////////////////////////
//  DicesData.cpp
//  Implementation of the Class DicesData
//  Created on:      16-май-2015 18:57:59
//  Original author: z510
///////////////////////////////////////////////////////////
#include "dicesdata.h"
#include <ctime>

//DicesData::DicesData()
//{

//}

DicesData::~DicesData()
{
    delete m_dice1;
    delete m_dice2;
    delete m_dice3;
    delete m_dice4;
    delete m_dice5;
}

// calc all needed data
void DicesData::calcData(){
    // TODO
}

// set amount of calculating dices
void DicesData::setDiceAmount(int amount){
    this->m_diceAmount = amount;
}

// get amount of dices
int DicesData::diceAmount(){

    return this->m_diceAmount;
}

// reset data of class
void DicesData::resetData(){
    // TODO
}

// create dices // is it necessary ???
void DicesData::createDices(){

}

// get dice by number ??? necessary?
Dice* DicesData::getDice(int number){
    switch (number) {
    case 1:
        return m_dice1;
    case 2:
        return m_dice2;
    case 3:
        return m_dice3;
    case 4:
        return m_dice4;
    case 5:
        return m_dice5;
    default:
        return NULL;
    }
}


void DicesData::generateNewCoords(bool d1, bool d2, bool d3, bool d4, bool d5){
    //TODO coordinates that didn't collide
    //bool x21,x31,x32,x41,x42,x43,x51,x52,x53,x54;
    //bool y21,y31,y32,y41,y42,y43,y51,y52,y53,y54;
    Dice *objDice1, *objDice2, *objDice3, *objDice4, *objDice5;
    if(d1){
        objDice1 = new Dice();
        objDice1->setCoordX(objDice1->randomCoordX());
        objDice1->setCoordY(objDice1->randomCoordY());
        this->setDice1(objDice1);
    }
    if(d2){
       // do{
        objDice2 = new Dice();
        objDice2->setCoordX(objDice2->randomCoordX());
        objDice2->setCoordY(objDice2->randomCoordY());
        this->setDice2(objDice2);

        //x21 = (m_dice2->getCoordX()-m_dice1->getCoordX())<36;
        //y21 = (m_dice2->getCoordY()-m_dice1->getCoordY())<36;
        //}while(x21 && y21);
    }
    if(d3){
        //do{
        objDice3 = new Dice();
        objDice3->setCoordX(objDice3->randomCoordX());
        objDice3->setCoordY(objDice3->randomCoordY());
        this->setDice3(objDice3);
        //x31 = (m_dice3->getCoordX()-m_dice1->getCoordX())<36;
        //y31 = (m_dice3->getCoordY()-m_dice1->getCoordY())<36;
        //x32 = (m_dice3->getCoordX()-m_dice2->getCoordX())<36;
        //y32 = (m_dice3->getCoordY()-m_dice2->getCoordY())<36;
        //}while(x31 && y31 && x32 && y32);
    }
    if(d4){
        //do{
        objDice4 = new Dice();
        objDice4->setCoordX(objDice4->randomCoordX());
        objDice4->setCoordY(objDice4->randomCoordY());
        this->setDice4(objDice4);
        //x41 = (m_dice4->getCoordX()-m_dice1->getCoordX())<36;
        //y41 = (m_dice4->getCoordY()-m_dice1->getCoordY())<36;
        //x42 = (m_dice4->getCoordX()-m_dice2->getCoordX())<36;
        //y42 = (m_dice4->getCoordY()-m_dice2->getCoordY())<36;
        //x43 = (m_dice4->getCoordX()-m_dice3->getCoordX())<36;
        //y43 = (m_dice4->getCoordY()-m_dice3->getCoordY())<36;
        //}while(x41 && y41 && x42 && y42 && x43 && y43);
    }
    if(d5){
        //do{
        objDice5 = new Dice();
        objDice5->setCoordX(objDice5->randomCoordX());
        objDice5->setCoordY(objDice5->randomCoordY());
        this->setDice5(objDice5);
        //x51 = (m_dice5->getCoordX()-m_dice1->getCoordX())<36;
        //y51 = (m_dice5->getCoordY()-m_dice1->getCoordY())<36;
        //x52 = (m_dice5->getCoordX()-m_dice2->getCoordX())<36;
        //y52 = (m_dice5->getCoordY()-m_dice2->getCoordY())<36;
        //x53 = (m_dice5->getCoordX()-m_dice3->getCoordX())<36;
        //y53 = (m_dice5->getCoordY()-m_dice3->getCoordY())<36;
        //x54 = (m_dice5->getCoordX()-m_dice4->getCoordX())<36;
        //y54 = (m_dice5->getCoordY()-m_dice4->getCoordY())<36;
        //}while(x51 && y51 && x52 && y52 && x53 && y53 && x54 && y54);
    }
}

void DicesData::generateNewValues(bool d1, bool d2, bool d3, bool d4, bool d5){
    if(d1){
        m_dice1->setValue(m_dice1->randomValue());
    }
    if(d2){
        m_dice2->setValue(m_dice2->randomValue());
    }
    if(d3){
        m_dice3->setValue(m_dice3->randomValue());
    }
    if(d4){
        m_dice4->setValue(m_dice4->randomValue());
    }
    if(d5){
        m_dice5->setValue(m_dice5->randomValue());
    }
}
Dice* DicesData::dice1()
{
    return m_dice1;
}

void DicesData::setDice1(Dice *dice1)
{
    delete m_dice1;
    m_dice1 = dice1;
}
Dice* DicesData::dice2()
{
    return m_dice2;
}

void DicesData::setDice2(Dice *dice2)
{
    delete m_dice2;
    m_dice2 = dice2;
}
Dice* DicesData::dice3()
{
    return m_dice3;
}

void DicesData::setDice3(Dice *dice3)
{
    delete m_dice3;
    m_dice3 = dice3;
}
Dice* DicesData::dice4()
{
    return m_dice4;
}

void DicesData::setDice4(Dice *dice4)
{
    delete m_dice4;
    m_dice4 = dice4;
}
Dice *DicesData::dice5()
{
    return m_dice5;
}

void DicesData::setDice5(Dice *dice5)
{
    delete m_dice5;
    m_dice5 = dice5;
}

QQmlListProperty<Dice> DicesData::dices() const
{

}



void DicesData::setDiceCoords()
{
    QObject * dice1 = engine->findChild<QObject*>("dice1");
    QObject * dice2 = engine->findChild<QObject*>("dice2");
    QObject * dice3 = engine->findChild<QObject*>("dice3");
    QObject * dice4 = engine->findChild<QObject*>("dice4");
    QObject * dice5 = engine->findChild<QObject*>("dice5");

    bool d1,d2,d3,d4,d5;

    d1 = !((dice1->property("isInStack")).toBool());
    d2 = !((dice2->property("isInStack")).toBool());
    d3 = !((dice3->property("isInStack")).toBool());
    d4 = !((dice4->property("isInStack")).toBool());
    d5 = !((dice5->property("isInStack")).toBool());

    generateNewCoords(d1,d2,d3,d4,d5);

    int qmlX1, qmlX2;
    int qmlY1, qmlY2;
    if(d1){
        qmlX1 = m_dice1->getCoordX();
        qmlY1 = m_dice1->getCoordY();
        dice1->setProperty("x",qmlX1);
        dice1->setProperty("y",qmlY1);
    }
    if(d2){
        qmlX2 = m_dice2->getCoordX();
        qmlY2 = m_dice2->getCoordY();
        dice2->setProperty("x",qmlX2);
        dice2->setProperty("y",qmlY2);
    }
    if(d3){
        dice3->setProperty("x",m_dice3->getCoordX());
        dice3->setProperty("y",m_dice3->getCoordY());
    }
    if(d4){
        dice4->setProperty("x",m_dice4->getCoordX());
        dice4->setProperty("y",m_dice4->getCoordY());
    }
    if(d5){
        dice5->setProperty("x",m_dice5->getCoordX());
        dice5->setProperty("y",m_dice5->getCoordY());
    }

}

void DicesData::setDiceValues(){
    QObject * dice1 = engine->findChild<QObject*>("dice1");
    QObject * dice2 = engine->findChild<QObject*>("dice2");
    QObject * dice3 = engine->findChild<QObject*>("dice3");
    QObject * dice4 = engine->findChild<QObject*>("dice4");
    QObject * dice5 = engine->findChild<QObject*>("dice5");

    bool d1,d2,d3,d4,d5;

    d1 = !((dice1->property("isInStack")).toBool());
    d2 = !((dice2->property("isInStack")).toBool());
    d3 = !((dice3->property("isInStack")).toBool());
    d4 = !((dice4->property("isInStack")).toBool());
    d5 = !((dice5->property("isInStack")).toBool());

    generateNewValues(d1,d2,d3,d4,d5);

    if(d1){
        dice1->setProperty("value",m_dice1->getValue());
    }
    if(d2){
        dice2->setProperty("value",m_dice2->getValue());
    }
    if(d3){
        dice3->setProperty("value",m_dice3->getValue());
    }
    if(d4){
        dice4->setProperty("value",m_dice4->getValue());
    }
    if(d5){
        dice5->setProperty("value",m_dice5->getValue());
    }
}





