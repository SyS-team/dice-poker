#ifndef DICESDATA_H
#define DICESDATA_H

#include <QQuickItem>
#include "dice.h"


class DicesData : public QQuickItem
{
    Q_OBJECT
    //Q_PROPERTY(Dice* dice1 READ dice1 WRITE setDice1 NOTIFY dice1Changed)
    //Q_PROPERTY(Dice* dice2 READ dice2 WRITE setDice2 NOTIFY dice2Changed)
    //Q_PROPERTY(Dice* dice3 READ dice3 WRITE setDice3 NOTIFY dice3Changed)
    //Q_PROPERTY(Dice* dice4 READ dice4 WRITE setDice4 NOTIFY dice4Changed)
    //Q_PROPERTY(Dice* dice5 READ dice5 WRITE setDice5 NOTIFY dice5Changed)
    //Q_PROPERTY(Dice dice6 READ dice6 WRITE setDice6 NOTIFY dice6Changed)
    Q_PROPERTY(int diceAmount READ diceAmount WRITE setDiceAmount NOTIFY diceAmountChanged)
    Q_PROPERTY(QQmlListProperty<Dice> dices READ dices)
    Q_CLASSINFO("DefaultProperty", "dices")
public:
    //DicesData();
    DicesData(QObject *QQMLObject) : engine(QQMLObject){
        m_dice1 = new Dice(0,0);
        m_dice2 = new Dice(0,0);
        m_dice3 = new Dice(0,0);
        m_dice4 = new Dice(0,0);
        m_dice5 = new Dice(0,0);
    }
    ~DicesData();
    //Dice *m_Dice; // ??? it's created automatically by Enterprise Architect

    void calcData();
    void setDiceAmount(int amount);
    int diceAmount();
    void createDices();
    Dice* getDice(int number);
    Q_INVOKABLE void generateNewCoords(bool d1, bool d2, bool d3, bool d4, bool d5);
    void generateNewValues(bool d1, bool d2, bool d3, bool d4, bool d5);
    //Q_INVOKABLE void testFunc(Dice* dice);

    Dice *dice1();
    void setDice1(Dice *dice1);
    Dice *dice2();
    void setDice2(Dice *dice2);
    Dice *dice3();
    void setDice3(Dice *dice3);
    Dice *dice4();
    void setDice4(Dice *dice4);
    Dice *dice5();
    void setDice5(Dice *dice5);


    QQmlListProperty<Dice> dices() const;

private:
    Dice *m_dice1;
    Dice *m_dice2;
    Dice *m_dice3;
    Dice *m_dice4;
    Dice *m_dice5;
    int m_diceAmount;

    QList<Dice*>m_dices;

    void resetData();

protected:
    QObject * engine; // need to get access to QML properties

public slots:
    void setDiceCoords();
    void setDiceValues();

signals:
    void dice1Changed();
    void dice2Changed();
    void dice3Changed();
    void dice4Changed();
    void dice5Changed();
    void diceAmountChanged();

public slots:
};

#endif // DICESDATA_H
