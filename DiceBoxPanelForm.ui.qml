import QtQuick 2.4

Rectangle {
    id: root
    height: 64
    color: "#fff59d"
    state: "First"

    property alias diceBoxState: root.state
    property alias diceBox: diceBox1
    width: 96+96

    DiceBox {
        id: diceBox1
    }
//    MouseArea {
//        anchors.fill: parent
//        onClicked: root.diceBoxState === "First" ? root.diceBoxState = "Second" : root.diceBoxState = "First"
//    }

    states: [
        State {
            name: "First"

            PropertyChanges {
                target: diceBox1
                x: 16
                y: 0
            }
        },
        State {
            name: "Second"

            PropertyChanges {
                target: diceBox1
                x: 112 // 96+16
                y: 0
            }
        }
    ]

    //color: "transparent"
}

