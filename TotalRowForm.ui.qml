import QtQuick 2.4

Item {
    id: root
    width: 304
    height: 24

    property int points1: 0
    property int points2: 0
    property string combName: qsTr("Total score") //: textCombName.text

    property alias textPoints1: textPoints1
    property alias textPoints2: textPoints2




    signal clickedPoints()
//    clickedPoints:
    FontLoader { id: fixedFont; source: "qrc:/fonts/Papyrus.ttf" }

    Text{
        id: textCombName
        width: 176-18-64
        //root.width*2/3
        text: root.combName
        styleColor: "#068709"
        style: Text.Outline
        font.italic: false
        font.bold: true
        font.pointSize: 12
        font.family: fixedFont.name
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 18 //"combin"
        // TODO styles
    }
    Text{
        id: textPoints1
        width: 96 //root.width/3
        anchors.left: textCombName.right
        text: root.points1
        font.pointSize: 12
        font.family: fixedFont.name
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        // TODO styles
    }
    Text{
        id: textPoints2
        width: 96 //root.width/3
        anchors.left: textPoints1.right
        text: root.points2
        font.pointSize: 12
        font.family: fixedFont.name
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        // TODO styles
    }
}

