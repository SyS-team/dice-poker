TEMPLATE = app

QT += qml quick

SOURCES += main.cpp \
    dice.cpp \
    dicesdata.cpp \
    combination.cpp

TARGET = "Dice Poker"

TRANSLATIONS += localisation/translation_ru.ts

lupdate_only {
SOURCES += StartPanel.qml \
            StartPanelForm.ui.qml \
            TotalRowForm.ui.qml \
            WinnerModalForm.ui.qml \
            NameRowForm.ui.qml \
            MenuPanelForm.ui.qml \
            CombinationPanelForm.ui.qml
}


RESOURCES += qml.qrc

RC_ICONS = dice.ico

deployment.files += qmldir

OTHER_FILES += $$deployment.files

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =


# Default rules for deployment.
include(deployment.pri)

HEADERS += dice.h \
    dicesdata.h \
    combination.h
