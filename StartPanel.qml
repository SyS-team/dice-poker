import QtQuick 2.4

//StartPanelForm {
ModalWindow{
        id: root
        closeable: false

        property string playerName1: qsTr("Player 1")
        property string playerName2: qsTr("Player 2")

        signal applyClicked()
        onApplyClicked: {}

        StartPanelForm {
            anchors.centerIn: parent
            buttonApply.onPressedUser: {
                var voidFields = checkVoidFields()
                if(voidFields === 0) applyEnter();
            }
            textField1.onAccepted: {
                var voidFields = checkVoidFields()
                if(voidFields === 0) applyEnter();
            }
            textField2.onAccepted: {
                var voidFields = checkVoidFields()
                if(voidFields === 0) applyEnter();
            }
            textField1.onFocusChanged: if(textField1.focus === false)checkVoidFields()
            textField2.onFocusChanged: if(textField2.focus === false)checkVoidFields()

            function checkVoidFields(){
                var ret = 0
                if(textField1.length === 0){
                    sign1.activate()
                    ret += 10
                }
                else{
                    sign1.deactivate()
                }

                if(textField2.length === 0){
                    sign2.activate()
                    ret += 01
                }
                else{
                    sign2.deactivate()
                }

                return ret
            }
            function applyEnter(){
                root.playerName1 = enteredName1
                root.playerName2 = enteredName2
                root.applyClicked()
                root.hide()
            }

        }
}

