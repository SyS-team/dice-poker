import QtQuick 2.4

Rectangle {
    id: root
    width: 304
    height: 128
    color: "transparent"
//    color: "#f57f17"

    property alias buttonStart: menuButton1
    property alias buttonGiveup: menuButton2
    property alias buttonRules: menuButton3
    property alias buttonAbout: menuButton4
    property alias buttonPreferences: menuButton5




//    Flow {
//        id: flow1
//        layoutDirection: Qt.LeftToRight
//        flow: Flow.LeftToRight
//        anchors.fill: parent
//        spacing: 16
//    }



    BorderImage {
        id: name
        anchors.fill: parent
        source: "qrc:/img/img/menu_panel.png"
        //width: 100; height: 100
        border.left: 14; border.top: 14
        border.right: 16; border.bottom: 17
    }

    // TODO correct positions for buttons
    Grid {
        id: grid1
        spacing: 16
        columns: 2
        anchors.fill: parent

        MenuButton {
            id: menuButton1
            text: qsTr("Start")
            anchors.leftMargin: 20
            anchors.topMargin: 20
            anchors.left: parent.left
            anchors.top: parent.top
        }

        MenuButton {
            id: menuButton2
            text: qsTr("Give Up")
            anchors.rightMargin: 20
            anchors.topMargin: 20
            anchors.top: parent.top
            anchors.right: parent.right
        }

        MenuButton {
            id: menuButton3
            text: qsTr("Rules")
            anchors.bottomMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.bottom: parent.bottom
        }

        MenuButton {
            id: menuButton4
            text: qsTr("About")
            anchors.bottomMargin: 20
            anchors.rightMargin: 20
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }

        MenuButton {
            id: menuButton5
            text: qsTr("Preferences")
            visible: false
        }
    }

}

