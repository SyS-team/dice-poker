#ifndef DICE_H
#define DICE_H

#include <QQuickItem>


#define PLAY_FIELD_X 304
#define PLAY_FIELD_Y 144//48+96
#define PLAY_FIELD_W 464
#define PLAY_FIELD_H 432
#define DICE_WIDTH 72
#define DICE_HEIGHT 72



class Dice : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(int coordX READ getCoordX WRITE setCoordX NOTIFY coordXChanged)
    Q_PROPERTY(int coordY READ getCoordY WRITE setCoordY NOTIFY coordYChanged)
    Q_PROPERTY(int value READ getValue WRITE setValue NOTIFY valueChanged)
public:
    Dice();
    Dice(int m_coordX, int m_coordY);
    ~Dice();

    void setCoordX(int x);
    void setCoordY(int y);
    void setValue(int m_value);
    void setUsed(bool used);
    int getCoordX();
    int getValue();
    int getCoordY();
    bool getUsed();
    int randomCoordX();
    int randomCoordY();
    int randomValue();



private:
    int m_coordX; // coord X on PlayField
    int m_coordY;
    int m_value; // value of dice (1..6)
    bool used; //??? is dice used in calculating


signals:
    void coordXChanged();
    void coordYChanged();
    void valueChanged();
};

#endif // DICE_H
