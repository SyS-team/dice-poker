import QtQuick 2.4

Rectangle {
    id: rectangle1
    width: 400
    height: 250
    color: "#00000000"
    property string winnerName: "Winner"
    property int score1: 100
    property int score2: 90
    property alias textWin: text1
    property alias textDraw: textDraw

    Image {
        id: image1
        anchors.fill: parent
        source: "qrc:/img/img/menu_panel.png"
    }

    Text {
        id: text1
        //~ Context Full string is "@playername win!" Ex: "Player win!"
        text: rectangle1.winnerName+qsTr(" win!", "winner message")
        anchors.top: parent.top
        anchors.topMargin: 10
        styleColor: "#ffffff"
        style: Text.Outline
        font.bold: true
        font.pointSize: 30
        font.family: "Courier"
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Text {
        id: textDraw
        visible: false
        text: qsTr("Round draw!")
        anchors.top: parent.top
        anchors.topMargin: 10
        styleColor: "#ffffff"
        style: Text.Outline
        font.bold: true
        font.pointSize: 30
        font.family: "Courier"
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Row {
        id: row1
        width: 200
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: text1.bottom

        Text {
            id: text2
            color: "#ffffff"
            text: rectangle1.score1
            anchors.rightMargin: 10
            styleColor: (rectangle1.score1>rectangle1.score2) ? "#00ff00" : "#ff0000"
            style: Text.Outline
            anchors.right: text4.left
            anchors.top: parent.top
            anchors.topMargin: 5
            font.pixelSize: 30
        }

        Text {
            id: text4
            color: "#ffffff"
            text: ":"
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 30
        }

        Text {
            id: text3
            color: "#ffffff"
            text: rectangle1.score2
            style: Text.Outline
            anchors.rightMargin: -(width+10)
            styleColor: (rectangle1.score1<rectangle1.score2) ? "#00ff00" : "#ff0000"
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.right: text4.right
            wrapMode: Text.WordWrap
            font.pixelSize: 30
        }

    }

}

