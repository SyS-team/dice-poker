import QtQuick 2.4

Rectangle {
    id: root
    width: 16
    height: 16
    color: "transparent"
    opacity: 0


    Image {
        id: attentionImage
        anchors.fill: parent
        opacity: root.opacity
        source: "qrc:/img/img/exclamation.png"
        //source: "img/exclamation.png"
    }
}
